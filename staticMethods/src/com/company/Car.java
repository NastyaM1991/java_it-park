package com.company;

public class Car
{
    private int id;
    private String name;
    private static int countAllCars = 0;

    public Car(String name)
    {
        countAllCars++;
        this.id = countAllCars;
        this.name = name;
    }

    public String getInfo()
    {
        return "Car Name: " + name;
    }

    public int getId()
    {
        return id;
    }

    public static int getCountAllCars()
    {
        return countAllCars;
    }
}
