package com.company;

public class BigInt
{
    private int[] digits;

    public BigInt(int countDigits)
    {
        digits = new int[countDigits];
        for (int i = 0; i < countDigits; i++)
        {
            digits[i] = 0;
        }
    }

    public BigInt(int[] digits)
    {
        this.digits = digits;
    }

    public String toString()
    {
        String output = "";

        for (int i = 0; i < digits.length; i++)
        {
            output += Integer.toString(digits[i]);
        }
        return output;
    }
}
