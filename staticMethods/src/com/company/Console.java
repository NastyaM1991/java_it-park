package com.company;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.io.IOException;
import java.util.Scanner;

public class Console
{
    public static void println(String string)
    {
        System.out.println(string);
    }

    public static void print(String string)
    {
        System.out.print(string);
    }

    public static int inputInt(String message)
    {
        int x = 0;
        boolean inputResult = false;

        do
        {
            try
            {
                Scanner scanner = new Scanner(System.in);
                System.out.println(message);
                x = scanner.nextInt();
                inputResult = true;
            } catch (Exception e)
            {
                inputResult = false;
            }
        }
        while (inputResult == false);

        return x;
    }

    public static char inputChar(String message)
    {
        char x = 0;
        boolean inputResult = false;

        do
        {
            try
            {
                System.out.println(message);
                x = (char) System.in.read();
                inputResult = true;
            } catch (Exception e)
            {
                inputResult = false;
            }
        }
        while (inputResult == false);

        return x;
    }

    public int read() throws IOException
    {
        return System.in.read();
    }


}
