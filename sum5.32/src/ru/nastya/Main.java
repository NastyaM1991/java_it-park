package ru.nastya;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int n;

        System.out.println("Input n");
        n = scanner.nextInt();

        if (n > 0)
        {
            double sum = 0;
            int i = 1;
            while (i <= n)
            {
                sum += 1.0 / i;
                i++;
            }
            System.out.println(sum);
        }
        else
        {
            System.out.println("!!!n > 0!!!");
        }
    }
}
