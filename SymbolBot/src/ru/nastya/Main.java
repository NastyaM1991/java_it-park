package ru.nastya;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        char symbol;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите символ: ");
        symbol = scanner.next().charAt(0);

        if (symbol >= 'а' && symbol <= 'я' || symbol >= 'А' && symbol <= 'Я')
        {
            System.out.print("Это буква ");
            if (symbol >= 'а' && symbol <= 'я')
            {
                System.out.print("маленькая");
            }
            else
            {
                System.out.print("большая");
            }
        }
        else if (symbol >= '0' && symbol <= '9')
        {
            System.out.print("Это цифра ");
            if (symbol % 2 == 0)
            {
                System.out.print("четная");
            }
            else
            {
                System.out.print("нечетная");
            }
        }
        else
        {
            System.out.print("Это неизвестный символ");
        }
    }
}
