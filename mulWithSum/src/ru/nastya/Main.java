package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int x;
        int y;
        int mul = 0;

        System.out.println("Input x");
        x = scanner.nextInt();

        System.out.println("Input y");
        y = scanner.nextInt();

        int i = 0;
        while (i < y)
        {
            mul += x;
            i++;
        }
        System.out.println(mul);

        i = 0;
        mul = 0;

        while (i < x)
        {
            mul += y;
            i++;
        }
        System.out.println(mul);
    }
}
