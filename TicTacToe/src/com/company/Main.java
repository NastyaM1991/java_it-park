package com.company;

import java.util.Scanner;

public class Main
{
    enum Cell
    {
        EMPTY,
        CROSS,
        ZERO
    }

    static void clearField(Cell[][] field)
    {
        for (int i = 0; i < field.length; i++)
        {
            for (int j = 0; j < field[i].length; j++)
            {
                field[i][j] = Cell.EMPTY;
            }
        }

        printField(field);
    }

    static void printField(Cell[][] field)
    {
        for (int i = 0; i < 50; i++)
        {
            System.out.println();
        }

        for (int i = 0; i < field.length; i++)
        {
            for (int j = 0; j < field[i].length; j++)
            {
                switch (field[i][j])
                {
                    case EMPTY:
                        System.out.print(".");
                        break;
                    case CROSS:
                        System.out.print("x");
                        break;
                    case ZERO:
                        System.out.print("o");
                        break;
                }
            }
            System.out.println();
        }
    }

    static boolean isNotFilled(Cell[][] field)
    {
        for (int i = 0; i < field.length; i++)
        {
            for (int j = 0; j < field[i].length; j++)
            {
                if (field[i][j] == Cell.EMPTY)
                {
                    return true;
                }
            }
        }
        return false;
    }

    static void turn(Cell[][] field, Cell whoseTurn)
    {
        Scanner scanner = new Scanner(System.in);

        int i;
        int j;

        System.out.println("Player " + whoseTurn + " turn");

        do
        {
            System.out.print("Input i: ");
            i = scanner.nextInt() - 1;

            System.out.print("Input j: ");
            j = scanner.nextInt() - 1;
        }
        while (i < 0 || j < 0 || i > field.length - 1 || j > field[0].length - 1 || field[i][j] != Cell.EMPTY);

        field[i][j] = whoseTurn;

        printField(field);
    }

    static boolean isLineWin(Cell[][] field, Cell whoseTurn, int i)
    {
        for (int j = 0; j < field[i].length; j++)
        {
            if (field[i][j] != whoseTurn)
            {
                return false;
            }
        }
        return true;
    }

    static boolean isColumnWin(Cell[][] field, Cell whoseTurn, int j)
    {
        for (int i = 0; i < field.length; i++)
        {
            if (field[i][j] != whoseTurn)
            {
                return false;
            }
        }
        return true;
    }

    static boolean isDiagonal1Win(Cell[][] field, Cell whoseTurn)
    {
        for (int i = 0; i < field.length; i++)
        {
            if (field[i][i] != whoseTurn)
            {
                return false;
            }
        }
        return true;
    }

    static boolean isDiagonal2Win(Cell[][] field, Cell whoseTurn)
    {
        for (int i = 0; i < field.length; i++)
        {
            if (field[i][field.length - 1 - i] != whoseTurn)
            {
                return false;
            }
        }
        return true;
    }

    static boolean isLinesWin(Cell[][] field, Cell whoseTurn)
    {
        for (int i = 0; i < field.length; i++)
        {
            if (isLineWin(field, whoseTurn, i))
            {
                return true;
            }
        }
        return false;
    }

    static boolean isColumnsWin(Cell[][] field, Cell whoseTurn)
    {
        for (int j = 0; j < field[0].length; j++)
        {
            if (isColumnWin(field, whoseTurn, j))
            {
                return true;
            }
        }
        return false;
    }

    static boolean isWin(Cell[][] field, Cell whoseTurn)
    {
        if (isLinesWin(field, whoseTurn)
                || isColumnsWin(field, whoseTurn)
                || isDiagonal1Win(field, whoseTurn)
                || isDiagonal2Win(field, whoseTurn))
        {
            return true;
        }

        return false;
    }

    static void printWinner(Cell winner)
    {
        if (winner == Cell.EMPTY)
        {
            System.out.println("DRAW!");
        }
        else
        {
            System.out.println(winner + " WIN!");
        }
    }

    static Cell changeTurn(Cell whoseTurn)
    {
        if (whoseTurn == Cell.CROSS)
        {
            whoseTurn = Cell.ZERO;
        }
        else
        {
            whoseTurn = Cell.CROSS;
        }

        return whoseTurn;
    }

    public static void main(String[] args)
    {
        final int fieldSize = 3;
        Cell[][] field = new Cell[fieldSize][fieldSize];

        clearField(field);

        Cell winner = Cell.EMPTY;
        Cell whoseTurn = Cell.CROSS;

        do
        {
            turn(field, whoseTurn);
            if (isWin(field, whoseTurn))
            {
                winner = whoseTurn;
                break;
            }

            whoseTurn = changeTurn(whoseTurn);
        }
        while (isNotFilled(field));

        printWinner(winner);
    }
}
