package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        double firstNum, secondNum, result;
        char sign;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Input first number ");
        firstNum = scanner.nextDouble();

        System.out.print("Input sign ");
        sign = scanner.next().charAt(0);

        System.out.print("Input second number ");
        secondNum = scanner.nextDouble();

        switch (sign)
        {
            case '+':
                result = firstNum + secondNum;
                System.out.print(result);
                break;
            case '-':
                result = firstNum - secondNum;
                System.out.print(result);
                break;
            case '*':
                result = firstNum * secondNum;
                System.out.print(result);
                break;
            case '/':
                if (secondNum == 0)
                {
                    System.out.print("you can't divide by 0");
                }
                else
                {
                    result = firstNum / secondNum;
                    System.out.print(result);
                }
                break;
            default:
                System.out.print("unknown command");
                break;
        }
    }
}
