package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class CardDeck
{
    static Random random = new Random(System.currentTimeMillis());
    private ArrayList<Card> cards;
    private int lastGivenCardIndex;

    public CardDeck(String fileName) throws IOException
    {
        loadCardsFromFile(fileName);
        Collections.shuffle(cards);
        lastGivenCardIndex = -1;
    }

    private void loadCardsFromFile(String fileName) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        cards = new ArrayList<Card>();

        String cardKindStr;
        while ((cardKindStr = reader.readLine()) != null)
        {
            CardKind cardKind = CardKind.valueOf(cardKindStr);
            String name = reader.readLine();
            int damage = Integer.parseInt(reader.readLine());
            int health = Integer.parseInt(reader.readLine());

            switch (cardKind)
            {
                case WIZARD:
                    cards.add(new WizardCard(name, damage, health));
                    break;
                case KNIGHT:
                    cards.add(new KnightCard(name, damage, health));
                    break;
                case DRAGON:
                    cards.add(new DragonCard(name, damage, health));
                    break;
                case ELF:
                    cards.add(new ElfCard(name, damage, health));
                    break;
            }
        }
        reader.close();
    }

    @Override
    public String toString()
    {
        String output = "";

        for (int i = 0; i < cards.size(); i++)
        {
            output += cards.get(i).toString() + "\n";

        }
        return output;
    }

    public boolean hasSpareCards()
    {
        return lastGivenCardIndex < (cards.size() - 1);
    }

    public Card getNextCard()
    {
        lastGivenCardIndex++;
        return cards.get(lastGivenCardIndex);
    }

    public int getAmountOfCards()
    {
        return cards.size();
    }
}
