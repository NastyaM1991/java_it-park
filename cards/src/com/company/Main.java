package com.company;

import java.io.IOException;
import java.util.Scanner;

public class Main
{


    public static void main(String[] args) throws IOException
    {
        Scanner scanner = new Scanner(System.in);
        /*
        //System.out.println(card1.getStringValue());

        CardsManager cards = new CardsManager(5);
        cards.addNewCard(2, card1);
        cards.addNewCard(0, card2);

        System.out.println(cards.getStringValue());

        //System.out.println(cards.getStringValue());*/

        CardDeck cardDeck = new CardDeck("Cards.txt");

        if (cardDeck.getAmountOfCards() != 0 && cardDeck.getAmountOfCards() % 2 == 0)
        {
            System.out.println(cardDeck);

            /*CardsManager playerCards = new CardsManager(cardDeck.getAmountOfCards() / 2);
            CardsManager compCards = new CardsManager(cardDeck.getAmountOfCards() / 2);*/

            CardsManager playerCards = new CardsManager();
            CardsManager compCards = new CardsManager();

            while (cardDeck.hasSpareCards())
            {
                playerCards.addNewCard(cardDeck.getNextCard());
                compCards.addNewCard(cardDeck.getNextCard());
            }

            System.out.println(playerCards);
            System.out.println(compCards);

            int round = 1;
            while (playerCards.hasCards() && compCards.hasCards())
            {
                System.out.println("Round " + round);
                int playerIndex;

                do
                {
                    System.out.println("Input card index");
                    playerIndex = scanner.nextInt();
                }
                while (!playerCards.hasCard(playerIndex));

                int compIndex = compCards.getRandomIndex();

                fight(playerCards, compCards, playerIndex, compIndex);
                round++;
            }

            if (playerCards.hasCards())
            {
                System.out.println("!!!Player wins!!!");
            }
            else if (compCards.hasCards())
            {
                System.out.println("!!!Comp wins!!!");
            }
            else
            {
                System.out.println("Draw");
            }
        }
        else
        {
            System.out.println("Bring Cards!");
        }
    }

    private static void fight(CardsManager playerCards, CardsManager compCards, int playerIndex, int compIndex)
    {
        Card player = playerCards.getCard(playerIndex);
        Card comp = compCards.getCard(compIndex);

        player.attack(comp);
        comp.attack(player);

        if (player.getHealth() <= 0)
        {
            System.out.println(player.getName() + " died");
            playerCards.deleteCard(playerIndex);
        }

        if (comp.getHealth() <= 0)
        {
            System.out.println(comp.getName() + " died");
            compCards.deleteCard(compIndex);
        }

        System.out.println(playerCards);
        System.out.println(compCards);
    }
}
