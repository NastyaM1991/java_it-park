package com.company;

public enum CardKind
{
    WIZARD,
    KNIGHT,
    DRAGON,
    ELF
}
