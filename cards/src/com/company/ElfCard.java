package com.company;

import java.util.Random;

public class ElfCard extends Card
{
    static Random random = new Random(System.currentTimeMillis());

    public ElfCard(String name, int damage, int health)
    {
        super(name, damage, health);
    }

    @Override
    public String toString()
    {
        return "Elf card - " + super.toString();
    }

    @Override
    public void attack(Card opponent)
    {
        int damage = getDamage();

        if (random.nextInt(3) == 0) //в 1/3 случаев удар эльфа будет в 1.5 раза мощнее
        {
            damage *= 1.5;
        }

        opponent.defend(damage);
    }

}
