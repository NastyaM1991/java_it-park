package com.company;

import java.util.Random;

public class WizardCard extends Card
{
    static Random random = new Random(System.currentTimeMillis());

    public WizardCard(String name, int damage, int health)
    {
        super(name, damage, health);
    }

    @Override
    public String toString()
    {
        return "Wizard card - " + super.toString();
    }

    @Override
    public void defend(int damage)
    {
        super.defend(damage - (int) (getHealth() * (random.nextInt(20) / 100.0))); //волшебник на 1%-20% восстанавливает свое здоровье
    }
}
