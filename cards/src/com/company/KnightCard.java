package com.company;

import java.util.Random;

public class KnightCard extends Card
{
    static Random random = new Random(System.currentTimeMillis());

    public KnightCard(String name, int damage, int health)
    {
        super(name, damage, health);
    }

    @Override
    public String toString()
    {
        return "Knight card - " + super.toString();
    }

    @Override
    public void defend(int damage)
    {
        super.defend(damage - (int) (getHealth() * (random.nextInt(15) / 100.0))); //рыцарь на 1%-15% восстанавливает свое здоровье
    }
}
