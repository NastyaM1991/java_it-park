package com.company;

import java.util.ArrayList;
import java.util.Random;

public class CardsManager
{
    private ArrayList<Card> cards;
    static Random random = new Random(System.currentTimeMillis());

    public CardsManager()
    {
        cards = new ArrayList<Card>();
    }

    public void addNewCard(int index, Card card)
    {
        if (index >= 0 && index < cards.size())
        {
            cards.add(index, card);
        }

        /*if (index >= 0 && index < cards.length)
        {
            cards[index] = card;
        }*/
    }

    public void addNewCard(Card card)
    {
        cards.add(card);

        /*for (int i = 0; i < cards.length; i++)
        {
            if (cards[i] == null)
            {
                cards[i] = card;
                break;
            }
        }*/
    }

    public void deleteCard(int index)
    {
        if (index >= 0 && index < cards.size())
        {
            cards.remove(index);
        }

        /*if (index >= 0 && index < cards.length)
        {
            cards[index] = null;
        }*/
    }

    public Card getCard(int index)
    {
        return cards.get(index);
        //return cards[index];
    }

    @Override
    public String toString()
    {
        String output = "";

        for (int i = 0; i < cards.size(); i++)
        {
            output += i + ". " + cards.get(i).toString() + "\n";
        }

        /*for (int i = 0; i < cards.length; i++)
        {
            if (cards[i] != null)
            {
                output += i + ". " + cards[i].toString() + "\n";
            }
        }*/
        return output;
    }

    public boolean hasCards()
    {
        return !cards.isEmpty();

        /*for (int i = 0; i < cards.length; i++)
        {
            if (cards[i] != null)
            {
                return true;
            }
        }
        return false;
         */
    }

    public boolean hasCard(int index)
    {
        if (index < 0 || index >= cards.size() || cards.get(index) == null)
        {
            return false;
        }
        else
        {
            return true;
        }

        /*if (index < 0 || index >= cards.length || cards[index] == null)
        {
            return false;
        }
        else
        {
            return true;
        }*/
    }

    public int getRandomIndex()
    {
        int index;

        do
        {
            //index = random.nextInt(cards.length);
            index = random.nextInt(cards.size());
        }
        while (!hasCard(index));

        return index;
    }

}
