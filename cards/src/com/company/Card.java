package com.company;

public class Card
{
    private String name;
    private int damage;
    private int health;

    public Card(String name, int damage, int health)
    {
        this.name = name;
        this.damage = damage;
        this.health = health;
    }

    public void attack(Card opponent)
    {
        opponent.defend(damage);
    }

    public void defend(int damage)
    {
        health -= damage;
    }

    public int getDamage()
    {
        return damage;
    }

    public int getHealth()
    {
        return health;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public String toString()
    {
        return "Name: " + name + " Damage: " + damage + " Health: " + health;
    }
}
