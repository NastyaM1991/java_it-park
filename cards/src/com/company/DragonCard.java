package com.company;

import java.util.Random;

public class DragonCard extends Card
{
    static Random random = new Random(System.currentTimeMillis());

    public DragonCard(String name, int damage, int health)
    {
        super(name, damage, health);
    }

    @Override
    public String toString()
    {
        return "Dragon card - " + super.toString();
    }

    @Override
    public void attack(Card opponent)
    {
        opponent.defend(getDamage() + random.nextInt(10)); //каждый удар дракона больше на 1..10
    }

}
