package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int a;
        int b;

        System.out.println("Input a");
        a = scanner.nextInt();

        System.out.println("Input b");
        b = scanner.nextInt();

        if (b >= a)
        {
            double sum = 0;
            int count = 0;
            int i = a;
            while (i <= b)
            {
                sum += i;
                count++;
                i++;
            }
            System.out.println(sum / count);
        }
        else
        {
            System.out.println("b >= a");
        }
    }
}
