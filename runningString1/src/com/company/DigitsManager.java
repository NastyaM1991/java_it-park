package com.company;

public class DigitsManager
{
    private String number;
    private Digit[] digits;

    public DigitsManager(int number)
    {
        this.number = Integer.toString(number, 10);
        convertToStringDigits();
    }

    public DigitsManager(String number)
    {
        this.number = number;
        filtrateNumber();
        convertToStringDigits();
    }

    private void filtrateNumber()
    {
        StringBuffer buffer = new StringBuffer(number);

        for (int i = 0; i < buffer.length(); i++)
        {
            if (!(buffer.charAt(i) >= '0' && buffer.charAt(i) <= '9'))
            {
                buffer.deleteCharAt(i);
                i = -1;
            }
        }

        number = buffer.toString();
    }

    public String getPlainNumber()
    {
        return number;
    }

    public String getFormatNumber()
    {
        String output = "";

        for (int i = 0; i < digits.length; i++)
        {
            output += digits[i].getStringValue() + "\n";
        }
        return output;
    }

    public Digit getDigitAt(int index)
    {
        return digits[index];
    }

    private void convertToStringDigits()
    {
        digits = new Digit[number.length()];
        for (int i = 0; i < digits.length; i++)
        {
            digits[i] = new Digit(number.charAt(i));
        }
    }
}
