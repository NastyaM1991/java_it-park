package com.company;

public class Main
{
    public static void main(String[] args)
    {
        Digit digit = new Digit(1);
        System.out.println(digit.getIntValue());
        System.out.println(digit.getStringValue());

        DigitsManager digitsManager = new DigitsManager("1234567890");
        /*System.out.println(digitsManager.getPlainNumber());
        System.out.println(digitsManager.getFormatNumber());*/
        System.out.println(digitsManager.getDigitAt(2).getStringValue());
    }
}
