package com.company;

public class Digit
{
    private char[][] symbols;
    private int digit;

    public Digit(int digit)
    {
        this.digit = digit;

        if (this.digit < 0 || this.digit > 9)
        {
            this.digit = 0;
        }

        convertToSymbols();
    }

    public Digit(char digit)
    {
        this.digit = digit - '0';

        if (this.digit < 0 || this.digit > 9)
        {
            this.digit = 0;
        }

        convertToSymbols();
    }

    public int getIntValue()
    {
        return digit;
    }

    public String getStringValue()
    {
        String output = "";
        for (int i = 0; i < symbols.length; i++)
        {
            for (int j = 0; j < symbols[i].length; j++)
            {
                output += symbols[i][j];
            }
            output += "\n";
        }
        return output;
    }

    private void convertToSymbols()
    {
        switch (digit)
        {
            case 0:
                symbols = new char[][]{
                        {'*','*','*'},
                        {'*',' ','*'},
                        {'*',' ','*'},
                        {'*',' ','*'},
                        {'*','*','*'}
                };
                break;
            case 1:
                symbols = new char[][]{
                        {' ',' ','*'},
                        {' ','*','*'},
                        {'*',' ','*'},
                        {' ',' ','*'},
                        {' ',' ','*'}
                };
                break;
            case 2:
                symbols = new char[][]{
                        {'*','*','*'},
                        {' ',' ','*'},
                        {' ','*',' '},
                        {'*',' ',' '},
                        {'*','*','*'}
                };
                break;
            case 3:
                symbols = new char[][]{
                        {'*','*','*'},
                        {' ',' ','*'},
                        {'*','*','*'},
                        {' ',' ','*'},
                        {'*','*','*'}
                };
                break;
            case 4:
                symbols = new char[][]{
                        {'*',' ','*'},
                        {'*',' ','*'},
                        {'*','*','*'},
                        {' ',' ','*'},
                        {' ',' ','*'}
                };
                break;
            case 5:
                symbols = new char[][]{
                        {'*','*','*'},
                        {'*',' ',' '},
                        {'*','*','*'},
                        {' ',' ','*'},
                        {'*','*','*'}
                };
                break;
            case 6:
                symbols = new char[][]{
                        {'*','*','*'},
                        {'*',' ',' '},
                        {'*','*','*'},
                        {'*',' ','*'},
                        {'*','*','*'}
                };
                break;
            case 7:
                symbols = new char[][]{
                        {'*','*','*'},
                        {' ',' ','*'},
                        {' ','*',' '},
                        {' ','*',' '},
                        {' ','*',' '}
                };
                break;
            case 8:
                symbols = new char[][]{
                        {'*','*','*'},
                        {'*',' ','*'},
                        {'*','*','*'},
                        {'*',' ','*'},
                        {'*','*','*'}
                };
                break;
            case 9:
                symbols = new char[][]{
                        {'*','*','*'},
                        {'*',' ','*'},
                        {'*','*','*'},
                        {' ',' ','*'},
                        {'*','*','*'}
                };
                break;
        }
    }
}
