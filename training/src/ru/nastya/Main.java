package ru.nastya;

public class Main {

    public static void main(String[] args) {
	// write your code here
        double distance = 10;
        double sum = 0;
        int days = 0;

        while (distance < 20)
        {
            distance += distance*10/100;
            days++;
        }
        System.out.println("days: " + days);

        days = 0;
        distance = 10;
        while (days < 20)
        {
            distance += distance*10/100;
            sum += distance;
            days++;
        }
        System.out.println("sum " + sum);

        days = 0;
        distance = 10;
        while (days < 10)
        {
            distance += distance*10/100;
            days++;
            System.out.println("day: " + days + " distance: " + distance);
        }
    }
}
