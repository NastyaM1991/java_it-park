package com.company;

public class Main
{

    public static void main(String[] args)
    {
        Circle c1 = new Circle(1, 1, 2);
        Square s1 = new Square(2, 3, 5, 5);
        Line l1 = new Line(4, 5, 10, 11);

        c1.draw();
        s1.draw();
        l1.draw();

        Figure[] figures = new Figure[3];
        figures[0] = new Circle(1, 1, 2);
        figures[1] = new Square(2, 3, 5, 5);
        figures[2] = new Line(4, 5, 10, 11);

        for (int i = 0; i < figures.length; i++)
        {
            figures[i].draw();
        }
    }
}
