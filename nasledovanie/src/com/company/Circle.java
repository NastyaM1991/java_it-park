package com.company;

public class Circle extends Figure
{
    private int r;

    public Circle(int x, int y, int r)
    {
        super(x, y);
        this.r = r;
    }

    @Override
    public void draw()
    {
        System.out.println("Circle: x = " + getX() + " y = " + getY() + " r = " + r);
    }
}
