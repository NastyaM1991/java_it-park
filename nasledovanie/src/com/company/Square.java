package com.company;

public class Square extends Figure
{
    private int h;
    private int w;

    public Square(int x, int y, int h, int w)
    {
        super(x, y);
        this.h = h;
        this.w = w;
    }

    @Override
    public void draw()
    {
        System.out.println("Square: x = " + getX() + " y = " + getY() + " h = " + h + " w = " + w);
    }
}
