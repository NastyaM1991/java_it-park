package ru.nastya;

public class Main
{

    public static void main(String[] args)
    {
        int i = 10;
        while (i <= 30)
        {
            System.out.println(i);
            i++;
        }
        System.out.println("--------------------");
        i = 10;
        do
        {
            System.out.println(i);
            i++;
        }
        while (i<=30);
    }
}
