package ru.nastya;

public class Main
{

    public static void main(String[] args)
    {
        int n = 100;
        double totalDistance = 0.0;
        double fromHome = 0.0;
        double currentDistance;
        int i = 1;

        while (i <= n)
        {
            currentDistance = 1.0 / i;
            totalDistance += currentDistance;
            if (i % 2 == 0)
            {
                fromHome -= currentDistance;
            }
            else
            {
                fromHome += currentDistance;
            }
            i++;
        }
        System.out.println("Total distance: " + totalDistance);
        System.out.println("From home: " + fromHome);
    }
}
