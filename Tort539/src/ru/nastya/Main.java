package ru.nastya;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N, kolvoRasresov;

        N = scanner.nextInt();
        if (N % 2 == 0)
        {
            kolvoRasresov = N/2;
        }
        else if(N == 1)
        {
            kolvoRasresov = 0;
        }
        else{
            kolvoRasresov = N;
        }
        System.out.println(kolvoRasresov);
    }
}
