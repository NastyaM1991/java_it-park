package ru.nastya;

import sun.util.resources.cldr.lag.CalendarData_lag_TZ;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int currentSalary;
        int currentSalaryPer3Months = 0;
        int sumPer1month = 0;
        int sumPer2month = 0;
        int sumPer3month = 0;
//        int Allsum = 0;

        for (int worker = 1; worker <= 12; worker++)
        {
            currentSalaryPer3Months = 0;
            for (int month = 1; month <= 3; month++)
            {
                System.out.println("Input " + worker + " salary " + month + " month");
                currentSalary = scanner.nextInt();
//                Allsum += currentSalary;
                currentSalaryPer3Months += currentSalary;
                if (month == 1)
                {
                    sumPer1month += currentSalary;
                }
                else if (month == 2)
                {
                    sumPer2month += currentSalary;
                }
                else
                {
                    sumPer3month += currentSalary;
                }
            }
            System.out.println("Worker " + worker + " got " + currentSalaryPer3Months);
        }
        System.out.println("Total sum = " + (sumPer1month + sumPer2month + sumPer3month));
        System.out.println("Per 1 month " + sumPer1month);
        System.out.println("Per 2 month " + sumPer2month);
        System.out.println("Per 3 month " + sumPer3month);
    }
}
