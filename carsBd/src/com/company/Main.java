package com.company;

import java.io.*;
import java.util.Scanner;

public class Main
{
    static Scanner scanner = new Scanner(System.in);

    static class Car
    {
        static int LastId = 0;

        enum Color
        {
            Black,
            White,
            Gray,
            Red,
            Green,
            Yellow,
            Custom
        }

        int Id;
        String Name;
        int Price;
        int IssueYear;
        Color BodyColor;

        Car(String Name, int Price, int IssueYear, Color BodyColor)
        {
            this.Name = Name;
            this.Price = Price;
            this.IssueYear = IssueYear;
            this.BodyColor = BodyColor;
        }

    }

    //--------------------System Functions---------------------//
    static Car[] ResizeArray(Car[] oldCars, int newLength)
    {
        Car[] newCars = new Car[newLength];

        int minLength = newLength < oldCars.length ? newLength : oldCars.length;

        for (int i = 0; i < minLength; i++)
        {
            newCars[i] = oldCars[i];
        }
        return newCars;
    }

    static void PrintMenu()
    {
        System.out.println("0 - Exit");
        System.out.println("1 - Add New Car");
        System.out.println("2 - Delete Car By Id");
        System.out.println("3 - Clear Array");
        System.out.println("4 - Update Car By Id");
        System.out.println("5 - Insert Car Into Position");
        System.out.println("6 - Print Car By Id");
        System.out.println("7 - Print Cars Between Min And Max Prices");
        System.out.println("8 - Print Cars Between Min And Max Issue Years");
        System.out.println("9 - Sort Cars By Id");
        System.out.println("10 - Sort Cars By Price");
        System.out.println("11 - Sort Cars By Issue Year");
        System.out.println("12 - Print Cars To txt File");
        System.out.println("13 - Save Cars To data File");
        System.out.println("14 - Load Cars From data File");
    }

    static int InputMenuPoint()
    {
        System.out.print("Input Menu Point: ");
        return scanner.nextInt();
    }

    static void Print50Enters()
    {
        for (int i = 0; i < 50; i++)
        {
            System.out.println();
        }
    }

    //--------------------System Functions---------------------//

    //--------------------CRUD Functions---------------------//
    static Car[] AddToEnd(Car[] cars, Car car)
    {
        if (cars == null)
        {
            cars = new Car[1];
        }
        else
        {
            cars = ResizeArray(cars, cars.length + 1);
        }
        cars[cars.length - 1] = car;

        return cars;
    }

    static Car[] DeleteById(Car[] cars, int id)
    {
        int indexDel = GetCarIndexById(cars, id);

        if (indexDel == -1)
        {
            System.out.println("Delete impossible. Element not found");
            return cars;
        }

        Car[] newCars = new Car[cars.length - 1];
        int newI = 0;

        for (int i = 0; i < cars.length; i++)
        {
            if (i != indexDel)
            {
                newCars[newI] = cars[i];
                newI++;
            }
        }

        return newCars;
    }

    static void UpdateCarById(Car[] cars, int id, Car car)
    {
        int indexUpd = GetCarIndexById(cars, id);

        if (indexUpd == -1)
        {
            System.out.println("Update impossible. Element not found");
            return;
        }

        car.Id = id;
        cars[indexUpd] = car;
    }

    static Car[] InsertCarIntoPosition(Car[] cars, int position, Car car)
    {
        if (position < 1 || position > cars.length)
        {
            System.out.println("Insert impossible. Element not found");
            return cars;
        }

        int indexIns = position - 1;

        Car[] newCars = new Car[cars.length + 1];
        int newI = 0;

        for (int i = 0; i < cars.length; i++)
        {
            if (newI != indexIns)
            {
                newCars[newI] = cars[i];
                newI++;
            }
            else
            {
                newCars[newI] = car;
                newI++;
                i--;
            }
        }

        return newCars;
    }
    //--------------------CRUD Functions---------------------//

    //--------------------Ext Functions---------------------//

    static void SortById(Car[] cars, boolean ASC)
    {
        boolean sort;
        Car temp;
        int offset = 0;

        do
        {
            sort = true;

            for (int i = 0; i < cars.length - 1 - offset; i++)
            {
                boolean condition;
                if (ASC)
                {
                    condition = cars[i + 1].Id < cars[i].Id;
                }
                else
                {
                    condition = cars[i + 1].Id > cars[i].Id;
                }
                if (condition)
                {
                    sort = false;
                    temp = cars[i];
                    cars[i] = cars[i + 1];
                    cars[i + 1] = temp;
                }
            }
            offset++;
        }
        while (!sort);
    }

    static void SortByPrice(Car[] cars, boolean ASC)
    {
        boolean sort;
        Car temp;
        int offset = 0;

        do
        {
            sort = true;

            for (int i = 0; i < cars.length - 1 - offset; i++)
            {
                boolean condition;
                if (ASC)
                {
                    condition = cars[i + 1].Price < cars[i].Price;
                }
                else
                {
                    condition = cars[i + 1].Price > cars[i].Price;
                }
                if (condition)
                {
                    sort = false;
                    temp = cars[i];
                    cars[i] = cars[i + 1];
                    cars[i + 1] = temp;
                }
            }
            offset++;
        }
        while (!sort);
    }

    static void SortByIssueYear(Car[] cars, boolean ASC)
    {
        boolean sort;
        Car temp;
        int offset = 0;

        do
        {
            sort = true;

            for (int i = 0; i < cars.length - 1 - offset; i++)
            {
                boolean condition;
                if (ASC)
                {
                    condition = cars[i + 1].IssueYear < cars[i].IssueYear;
                }
                else
                {
                    condition = cars[i + 1].IssueYear > cars[i].IssueYear;
                }
                if (condition)
                {
                    sort = false;
                    temp = cars[i];
                    cars[i] = cars[i + 1];
                    cars[i + 1] = temp;
                }
            }
            offset++;
        }
        while (!sort);
    }

    static int GetMinPrice(Car[] cars)
    {
        int minPrice = cars[0].Price;

        for (int i = 0; i < cars.length; i++)
        {
            if (cars[i].Price < minPrice)
            {
                minPrice = cars[i].Price;
            }
        }
        return minPrice;
    }

    static int GetMaxPrice(Car[] cars)
    {
        int maxPrice = cars[0].Price;

        for (int i = 0; i < cars.length; i++)
        {
            if (cars[i].Price > maxPrice)
            {
                maxPrice = cars[i].Price;
            }
        }
        return maxPrice;
    }

    static Car[] GetCarsBetweenMaxMinPrice(Car[] cars, int minPrice, int maxPrice)
    {
        Car[] foundCars = null;

        for (int i = 0; i < cars.length; i++)
        {
            if (cars[i].Price >= minPrice && cars[i].Price <= maxPrice)
            {
                foundCars = AddToEnd(foundCars, cars[i]);
            }
        }
        return foundCars;
    }

    static int GetMinIssueYear(Car[] cars)
    {
        int minIssueYear = cars[0].IssueYear;

        for (int i = 0; i < cars.length; i++)
        {
            if (cars[i].IssueYear < minIssueYear)
            {
                minIssueYear = cars[i].IssueYear;
            }
        }
        return minIssueYear;
    }

    static int GetMaxIssueYear(Car[] cars)
    {
        int maxIssueYear = cars[0].IssueYear;

        for (int i = 0; i < cars.length; i++)
        {
            if (cars[i].IssueYear > maxIssueYear)
            {
                maxIssueYear = cars[i].IssueYear;
            }
        }
        return maxIssueYear;
    }

    static Car[] GetCarsBetweenMaxMinIssueYear(Car[] cars, int minIssueYear, int maxIssueYear)
    {
        Car[] foundCars = null;

        for (int i = 0; i < cars.length; i++)
        {
            if (cars[i].IssueYear >= minIssueYear && cars[i].IssueYear <= maxIssueYear)
            {
                foundCars = AddToEnd(foundCars, cars[i]);
            }
        }
        return foundCars;
    }

    static void PrintSingle(Car car)
    {
        System.out.printf("%-4d%-12s%-10d%-10d%-10s\n", car.Id, car.Name, car.Price, car.IssueYear, car.BodyColor);
    }

    static void PrintAll(Car[] cars)
    {
        System.out.printf("%-4s%-12s%-10s%-10s%-10s\n", "Id", "Name", "Price", "IssueYear", "BodyColor");

        if (cars == null)
        {
            System.out.println("Array is empty");
        }
        else if (cars.length == 0)
        {
            System.out.println("Array is empty");
        }
        else
        {
            for (int i = 0; i < cars.length; i++)
            {
                PrintSingle(cars[i]);
            }
        }
        System.out.println("------------------------------");
    }

    static Car GetCarById(Car[] cars, int id)
    {
        int indexPrt = GetCarIndexById(cars, id);

        if (indexPrt == -1)
        {
            return null;
        }

        return cars[indexPrt];
    }

    static Car CreateCar(boolean newId)
    {
        System.out.print("Input Name: ");
        String name = scanner.next();

        System.out.print("Input Price: ");
        int price = scanner.nextInt();

        System.out.print("Input Issue Year: ");
        int issueYear = scanner.nextInt();

        System.out.print("Input Body Color: ");
        Car.Color bodyColor = Car.Color.valueOf(scanner.next());

        Car car = new Car(name, price, issueYear, bodyColor);

        if (newId)
        {
            Car.LastId++;
            car.Id = Car.LastId;
        }

        return car;
    }

    static int GetCarIndexById(Car[] cars, int id)
    {
        for (int i = 0; i < cars.length; i++)
        {
            if (cars[i].Id == id)
            {
                return i;
            }
        }
        return -1;
    }

    //--------------------Ext Functions---------------------//

    //--------------------File Functions---------------------//
    static void PrintAllToFile(Car[] cars, String fileName) throws FileNotFoundException
    {
        PrintStream stream = new PrintStream(fileName);

        stream.printf("%-4s%-12s%-10s%-10s%-10s", "Id", "Name", "Price", "IssueYear", "BodyColor");
        stream.println();

        if (cars == null)
        {
            stream.println("Array is empty");
        }
        else if (cars.length == 0)
        {
            stream.println("Array is empty");
        }
        else
        {
            for (int i = 0; i < cars.length; i++)
            {
                stream.printf("%-4d%-12s%-10d%-10d%-10s", cars[i].Id, cars[i].Name, cars[i].Price, cars[i].IssueYear, cars[i].BodyColor);
                stream.println();
            }
        }

        stream.close();
    }

    static void SaveCarsToFile(Car[] cars, String fileName) throws FileNotFoundException
    {
        PrintStream stream = new PrintStream(fileName);

        stream.println(cars.length);
        stream.println(Car.LastId);

        for (int i = 0; i < cars.length; i++)
        {
            stream.println(cars[i].Id);
            stream.println(cars[i].Name);
            stream.println(cars[i].Price);
            stream.println(cars[i].IssueYear);
            stream.println(cars[i].BodyColor);
        }

        stream.close();
    }

    static Car[] LoadCarsFromFile(String fileName) throws IOException
    {
        Car[] cars;
        int countCars;

        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        countCars = Integer.parseInt(reader.readLine());
        Car.LastId = Integer.parseInt(reader.readLine());

        cars = new Car[countCars];

        for (int i = 0; i < cars.length; i++)
        {
            int id = Integer.parseInt(reader.readLine());
            String name = reader.readLine();
            int price = Integer.parseInt(reader.readLine());
            int issueYear = Integer.parseInt(reader.readLine());
            Car.Color bodyColor = Car.Color.valueOf(reader.readLine());

            cars[i] = new Car(name, price, issueYear, bodyColor);
            cars[i].Id = id;
        }

        reader.close();
        return cars;
    }
    //--------------------File Functions---------------------//

    public static void main(String[] args) throws IOException
    {
        Car[] cars = null;
        int menuPoint;

        Car car;
        int id;
        boolean ASC;
        String fileName;

        do
        {
            Print50Enters();
            PrintAll(cars);
            PrintMenu();

            menuPoint = InputMenuPoint();

            switch (menuPoint)
            {
                case 1:
                    car = CreateCar(true);
                    cars = AddToEnd(cars, car);
                    break;
                case 2:
                    System.out.print("Input id: ");
                    id = scanner.nextInt();
                    cars = DeleteById(cars, id);
                    break;
                case 3:
                    cars = null;
                    break;
                case 4:
                    System.out.print("Input id: ");
                    id = scanner.nextInt();

                    car = CreateCar(false);

                    UpdateCarById(cars, id, car);
                    break;
                case 5:
                    System.out.print("Input position: ");
                    int position = scanner.nextInt();

                    car = CreateCar(true);

                    cars = InsertCarIntoPosition(cars, position, car);
                    break;
                case 6:
                    System.out.print("Input id: ");
                    id = scanner.nextInt();
                    car = GetCarById(cars, id);

                    if (car == null)
                    {
                        System.out.println("Print impossible. Element not found");
                    }
                    else
                    {
                        PrintSingle(car);
                    }
                    break;
                case 7:
                    System.out.println("Input min and max prices between " + GetMinPrice(cars) + " and " + GetMaxPrice(cars));

                    System.out.print("Min: ");
                    int minPrice = scanner.nextInt();

                    System.out.print("Max: ");
                    int maxPrice = scanner.nextInt();

                    PrintAll(GetCarsBetweenMaxMinPrice(cars, minPrice, maxPrice));
                    break;
                case 8:
                    System.out.println("Input min and max Issue Years between " + GetMinIssueYear(cars) + " and " + GetMaxIssueYear(cars));

                    System.out.print("Min: ");
                    int minIssueYear = scanner.nextInt();

                    System.out.print("Max: ");
                    int maxIssueYear = scanner.nextInt();

                    PrintAll(GetCarsBetweenMaxMinIssueYear(cars, minIssueYear, maxIssueYear));
                    break;
                case 9:
                    System.out.print("In ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    SortById(cars, ASC);
                    break;
                case 10:
                    System.out.print("In ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    SortByPrice(cars, ASC);
                    break;
                case 11:
                    System.out.print("In ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    SortByIssueYear(cars, ASC);
                    break;
                case 12:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    PrintAllToFile(cars, fileName);
                    break;
                case 13:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    SaveCarsToFile(cars, fileName);
                    break;
                case 14:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    cars = LoadCarsFromFile(fileName);
                    break;
                case 0:
                    System.out.println("Program will be finished");
                    break;
                default:
                    System.out.println("Unknown command");
                    break;
            }
            System.in.read();
        }
        while (menuPoint != 0);
    }
}
