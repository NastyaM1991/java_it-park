package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int number;
        int max;
        int min;
        int currentDigit;

        System.out.println("Input number");
        number = scanner.nextInt();

        max = number % 10;
        min = number % 10;

        while (number != 0)
        {
            currentDigit = number % 10;

            if (currentDigit > max)
            {
                max = currentDigit;
            }
            if (currentDigit < min)
            {
                min = currentDigit;
            }
            number /= 10;
        }

        if ((max - min) % 2 == 0)
        {
            System.out.println("Chet");
        }
        else
        {
            System.out.println("Nechet");
        }
    }
}
