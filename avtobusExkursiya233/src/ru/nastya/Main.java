package ru.nastya;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int currentHeight = 0;
        int number;
        boolean crash = false;
        int numberOfCrash = 0;

        number = scanner.nextInt();

        int i = 1;
        while (i <= number)
        {
            currentHeight = scanner.nextInt();

            if (currentHeight <= 437 && numberOfCrash == 0)
            {
                crash = true;
                numberOfCrash = i;
            }
            i++;
        }

        if (crash == false)
        {
            System.out.println("No crash");
        }
        else
        {
            System.out.println("Crash " + numberOfCrash);
        }
    }
}
