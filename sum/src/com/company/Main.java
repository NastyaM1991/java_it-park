package com.company;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int firstLength;
        int secondLength;
        int sumLength;
        int[] firstNumber;
        int[] secondNumber;
        int[] sum;

        System.out.println("Input first length");
        firstLength = scanner.nextInt();
        firstNumber = new int[firstLength];

        for (int i = 0; i < firstLength; i++)
        {
            System.out.println("Input " + (i + 1) + " digit");
            firstNumber[i] = scanner.nextInt();
        }

        System.out.println("Input second length");
        secondLength = scanner.nextInt();
        secondNumber = new int[secondLength];

        for (int i = 0; i < secondLength; i++)
        {
            System.out.println("Input " + (i + 1) + " digit");
            secondNumber[i] = scanner.nextInt();
        }

        if (firstLength > secondLength)
        {
            sumLength = firstLength + 1;
        }
        else
        {
            sumLength = secondLength + 1;
        }
        sum = new int[sumLength];

        int currentSum;
        int digitFirst;
        int digitSecond;
        for (int i = sumLength - 1; i > 0; i--)
        {
            firstLength--;
            secondLength--;
            if (firstLength < 0)
            {
                digitFirst = 0;
            }
            else
            {
                digitFirst = firstNumber[firstLength];
            }

            if (secondLength < 0)
            {
                digitSecond = 0;
            }
            else
            {
                digitSecond = secondNumber[secondLength];
            }
            currentSum = digitFirst + digitSecond;
            if (currentSum > 9)
            {
                sum[i] += currentSum % 10;
                sum[i - 1] += currentSum / 10;
            }
            else
            {
                sum[i] += currentSum;
            }
        }

        for (int i = 0; i < sumLength; i++)
        {
            if (i == 0 && sum[i] == 0)
            {
                continue;
            }
            System.out.print(sum[i]);
        }
    }
}
