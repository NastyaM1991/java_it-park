package ru.nastya;

public class Main
{

    public static void main(String[] args)
    {
        int n = 12;
        int[] mas = new int[n];
        for (int i = 0; i < n; i++)
        {
            mas[i] = i + 1;
        }
        for (int i = 0; i < n; i++)
        {
            System.out.print(mas[i] + " ");
        }
    }
}
