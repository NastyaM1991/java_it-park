package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int firstNumber = 0;
        int secondNumber;
        boolean is2numbers = false;

        int i = 0;
        while (i < 15)
        {
            System.out.println("Input " + (i + 1) + " number");
            if (is2numbers == false)
            {
                firstNumber = scanner.nextInt();
                is2numbers = true;
            }
            else
            {
                secondNumber = scanner.nextInt();
                if (firstNumber == secondNumber)
                {
                    System.out.println(firstNumber + " " + secondNumber);
                }
                else
                {
                    is2numbers = false;
                }
            }
            i++;
        }
    }
}
