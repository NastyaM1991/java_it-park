package ru.nastya;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int userNumber;
        int compNumber;
        int leftSide;
        int rightSide;
        int countTries;

        System.out.println("Input left side");
        leftSide = scanner.nextInt();

        System.out.println("Input right side");
        rightSide = scanner.nextInt();

        do
        {
            System.out.println("Input your number");
            userNumber = scanner.nextInt();
        }
        while (userNumber<leftSide || userNumber>rightSide);

        countTries = 0;
        do
        {
            countTries++;
//            compNumber = (int) Math.round(Math.random()*(rightSide - leftSide)+leftSide);
            compNumber = (rightSide+leftSide)/2;
            System.out.println("Try: "+ countTries + " Comp number: " + compNumber + " left side: " + leftSide + " right side: " + rightSide);

            if (compNumber > userNumber)
            {
                rightSide = compNumber;
            }
            else if(compNumber < userNumber)
            {
                leftSide = compNumber;
            }
        }
        while (compNumber != userNumber);

        System.out.println("DONE");
    }
}