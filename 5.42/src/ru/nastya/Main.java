package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int currentMark;
        int sum = 0;

        for (int i = 0; i < 4; i++)
        {
            System.out.println("Input " + (i + 1) + " mark ");
            currentMark = scanner.nextInt();
            sum += currentMark;
        }
        System.out.println("Sum = " + sum);
    }
}
