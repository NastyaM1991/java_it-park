package ru.nastya;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.security.spec.RSAOtherPrimeInfo;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean isPrime = true;

        System.out.println("Input number");
        int number = scanner.nextInt();

        int sqrtNumber = (int) Math.round(Math.sqrt(number)) + 1;
        for (int i = 2; i < sqrtNumber; i++)
        {
            if (number % i == 0)
            {
                isPrime = false;
                break;
            }
        }
        if (isPrime == true)
        {
            System.out.println("Prime");
        }
        else
        {
            System.out.printf("Not prime");
        }
    }
}
