package com.company;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");

        final char SECRET_LETTER = '*';
        String question;
        int countLetters;
        char[] secretWord;
        char[] guessWord;
        char currentLetter;
        boolean hasSecretLetter;
        boolean hasCurrentLetter;

        System.out.println("Input count letters");
        countLetters = scanner.nextInt();

        secretWord = new char[countLetters];
        guessWord = new char[countLetters];

        for (int i = 0; i < countLetters; i++)
        {
            guessWord[i] = SECRET_LETTER;
        }

        System.out.println("Input your word (letter by letter)");
        for (int i = 0; i < countLetters; i++)
        {
            secretWord[i] = scanner.next().charAt(0);
        }

        System.out.println("Input your question");
        question = scanner.next();

        for (int i = 0; i < 50; i++)
        {
            System.out.println();
        }

        do
        {
            hasSecretLetter = false;
            hasCurrentLetter = false;

            System.out.println(question);
            for (int i = 0; i < countLetters; i++)
            {
                System.out.print(guessWord[i]);
            }
            System.out.println();

            System.out.println("Input your letter: ");
            currentLetter = scanner.next().charAt(0);

            for (int i = 0; i < countLetters; i++)
            {
                if (currentLetter == secretWord[i])
                {
                    hasCurrentLetter = true;
                    guessWord[i] = currentLetter;
                }
            }

            if (hasCurrentLetter == true)
            {
                System.out.println("We have got your letter");
            }
            else
            {
                System.out.println("We haven't got your letter((");
            }

            for (int i = 0; i < countLetters; i++)
            {
                if (guessWord[i] == SECRET_LETTER)
                {
                    hasSecretLetter = true;
                }
            }
        }
        while (hasSecretLetter == true);

        for (int i = 0; i < countLetters; i++)
        {
            System.out.print(guessWord[i]);
        }
        System.out.println();

        System.out.println("!!!YOU WIN!!!");
    }
}
