package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int days;
        days = scanner.nextInt();

        String firstPlace = "G";
        String secondPlace = "C";
        String thirdPlace = "V";
        String temp;

        for (int i = 0; i < days; i++)
        {
            temp = thirdPlace;
            thirdPlace = secondPlace;
            secondPlace = temp;

            temp = firstPlace;
            firstPlace = secondPlace;
            secondPlace = temp;
        }
        System.out.println(firstPlace + secondPlace + thirdPlace);
    }
}
