package com.company;

import java.io.*;
import java.util.Scanner;

public class Main
{
    static Scanner scanner = new Scanner(System.in);

    static class NobelLaureate
    {
        static int LastId = 0;

        enum Science
        {
            Maths,
            Physics,
            Biology,
            Geography,
            Economy,
            Art
        }

        int id;
        String name;
        String surname;
        String patronymic;
        int birthYear;
        char gender;
        double rating;
        Science science;

        NobelLaureate(String name, String surname, String patronymic, int birthYear, char gender, double rating, Science science)
        {
            this.name = name;
            this.surname = surname;
            this.patronymic = patronymic;
            this.birthYear = birthYear;
            this.gender = gender;
            this.rating = rating;
            this.science = science;
        }
    }

    //--------------------System Functions---------------------//

    static void printMenu()
    {
        System.out.println("0 - Exit");
        System.out.println("1 - Add New Laureate");
        System.out.println("2 - Print Nobel Laureates To txt File");
        System.out.println("3 - Save Nobel Laureates To data File");
        System.out.println("4 - Load Nobel Laureates From data File");
        System.out.println("5 - Sort Nobel Laureates By Birth Year");
        System.out.println("6 - Print Male Nobel Laureates");
        System.out.println("7 - Print Female Nobel Laureates");
        System.out.println("8 - Print Nobel Laureates Between Min and Max Rating");
    }

    static int getMenuPoint()
    {
        int menuPoint;
        System.out.print("Input menu point: ");
        menuPoint = scanner.nextInt();
        return menuPoint;
    }

    static void print50Enters()
    {
        for (int i = 0; i < 50; i++)
        {
            System.out.println();
        }
    }

    static NobelLaureate[] resizeArray(NobelLaureate[] oldLaureates, int newLength)
    {
        NobelLaureate[] newLaureates = new NobelLaureate[newLength];

        int minLength = newLength < oldLaureates.length ? newLength : oldLaureates.length;

        for (int i = 0; i < minLength; i++)
        {
            newLaureates[i] = oldLaureates[i];
        }
        return newLaureates;
    }
    //--------------------System Functions---------------------//

    //--------------------CRUD Functions---------------------//
    static NobelLaureate[] addToEnd(NobelLaureate[] laureates, NobelLaureate laureate)
    {
        if (laureates == null)
        {
            laureates = new NobelLaureate[1];
        }
        else
        {
            laureates = resizeArray(laureates, laureates.length + 1);
        }
        laureates[laureates.length - 1] = laureate;

        return laureates;
    }

    //--------------------CRUD Functions---------------------//

    //--------------------Ext Functions---------------------//

    static double getMinRating(NobelLaureate[] laureates)
    {
        double minRating = laureates[0].rating;

        for (int i = 0; i < laureates.length; i++)
        {
            if (laureates[i].rating < minRating)
            {
                minRating = laureates[i].rating;
            }
        }
        return minRating;
    }

    static double getMaxRating(NobelLaureate[] laureates)
    {
        double maxRating = laureates[0].rating;

        for (int i = 0; i < laureates.length; i++)
        {
            if (laureates[i].rating > maxRating)
            {
                maxRating = laureates[i].rating;
            }
        }
        return maxRating;
    }

    static NobelLaureate[] getLaureatesBetweenMinMaxRating(NobelLaureate[] laureates, double minRating, double maxRating)
    {
        NobelLaureate[] foundLaureates = null;

        for (int i = 0; i < laureates.length; i++)
        {
            if (laureates[i].rating >= minRating && laureates[i].rating <= maxRating)
            {
                foundLaureates = addToEnd(foundLaureates, laureates[i]);
            }
        }
        return foundLaureates;
    }

    static NobelLaureate createLaureate(boolean newId)
    {
        System.out.print("Input Name: ");
        String name = scanner.next();

        System.out.print("Input Surname: ");
        String surname = scanner.next();

        System.out.print("Input Patronymic: ");
        String patronymic = scanner.next();

        System.out.print("Input Birth Year: ");
        int birthYear = scanner.nextInt();

        System.out.print("Input Laureate Gender: ");
        char gender = scanner.next().charAt(0);

        System.out.print("Input Laureate Rating: ");
        double rating = scanner.nextDouble();

        System.out.print("Input Science: ");
        NobelLaureate.Science science = NobelLaureate.Science.valueOf(scanner.next());

        NobelLaureate laureate = new NobelLaureate(name, surname, patronymic, birthYear, gender, rating, science);

        if (newId)
        {
            NobelLaureate.LastId++;
            laureate.id = NobelLaureate.LastId;
        }

        return laureate;
    }


    static void printSingle(NobelLaureate laureate)
    {
        System.out.printf("%-4d%-10s%-10s%-15s%-10d%-7c%-10.2f%-10s\n", laureate.id, laureate.name, laureate.surname, laureate.patronymic, laureate.birthYear, laureate.gender, laureate.rating, laureate.science);
    }

    static void printAll(NobelLaureate[] laureates)
    {
        System.out.printf("%-4s%-10s%-10s%-15s%-10s%-7s%-10s%-10s\n", "Id", "Name", "Surname", "Patronymic", "BirthYear", "Gender", "Rating", "Science");

        if (laureates == null || laureates.length == 0)
        {
            System.out.println("Array is empty");
        }
        else
        {
            for (int i = 0; i < laureates.length; i++)
            {
                printSingle(laureates[i]);
            }
        }
        System.out.println("------------------------------");
    }

    static NobelLaureate[] findMaleLaureates(NobelLaureate[] laureates)
    {
        NobelLaureate[] newLaureates = null;

        for (int i = 0; i < laureates.length; i++)
        {
            if (laureates[i].gender == 'M')
            {
                newLaureates = addToEnd(newLaureates, laureates[i]);
            }
        }
        return newLaureates;
    }

    static NobelLaureate[] findFemaleLaureates(NobelLaureate[] laureates)
    {
        NobelLaureate[] newLaureates = null;

        for (int i = 0; i < laureates.length; i++)
        {
            if (laureates[i].gender == 'F')
            {
                newLaureates = addToEnd(newLaureates, laureates[i]);
            }
        }
        return newLaureates;
    }

    static void sortByBirthYear(NobelLaureate[] laureates, boolean ASC)
    {
        boolean sort;
        NobelLaureate temp;
        int offset = 0;

        do
        {
            sort = true;

            for (int i = 0; i < laureates.length - 1 - offset; i++)
            {
                boolean condition;
                if (ASC)
                {
                    condition = laureates[i + 1].birthYear < laureates[i].birthYear;
                }
                else
                {
                    condition = laureates[i + 1].birthYear > laureates[i].birthYear;
                }
                if (condition)
                {
                    sort = false;
                    temp = laureates[i];
                    laureates[i] = laureates[i + 1];
                    laureates[i + 1] = temp;
                }
            }
            offset++;
        }
        while (!sort);
    }

    //--------------------Ext Functions---------------------//

    //--------------------File Functions---------------------//

    static void printAllToFile(NobelLaureate[] laureates, String fileName) throws FileNotFoundException
    {
        PrintStream stream = new PrintStream(fileName);

        stream.printf("%-4s%-10s%-10s%-15s%-10s%-7s%-10s%-10s\n", "Id", "Name", "Surname", "Patronymic", "BirthYear", "Gender", "Rating", "Science");
        stream.println();

        if (laureates == null || laureates.length == 0)
        {
            stream.println("Array is empty");
        }
        else
        {
            for (int i = 0; i < laureates.length; i++)
            {
                stream.printf("%-4d%-10s%-10s%-15s%-10d%-7c%-10.2f%-10s\n", laureates[i].id, laureates[i].name, laureates[i].surname, laureates[i].patronymic, laureates[i].birthYear, laureates[i].gender, laureates[i].rating, laureates[i].science);
                stream.println();
            }
        }
        stream.close();
    }

    static void saveLaureatesToFile(NobelLaureate[] laureates, String fileName) throws FileNotFoundException
    {
        PrintStream stream = new PrintStream(fileName);

        stream.println(laureates.length);
        stream.println(NobelLaureate.LastId);

        for (int i = 0; i < laureates.length; i++)
        {
            stream.println(laureates[i].id);
            stream.println(laureates[i].name);
            stream.println(laureates[i].surname);
            stream.println(laureates[i].patronymic);
            stream.println(laureates[i].birthYear);
            stream.println(laureates[i].gender);
            stream.println(laureates[i].rating);
            stream.println(laureates[i].science);
        }

        stream.close();
    }

    static NobelLaureate[] loadLaureatesFromFile(String fileName) throws IOException
    {
        NobelLaureate[] laureates;
        int countLaureates;

        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        countLaureates = Integer.parseInt(reader.readLine());
        NobelLaureate.LastId = Integer.parseInt(reader.readLine());

        laureates = new NobelLaureate[countLaureates];

        for (int i = 0; i < laureates.length; i++)
        {
            int id = Integer.parseInt(reader.readLine());
            String name = reader.readLine();
            String surname = reader.readLine();
            String patronymic = reader.readLine();
            int birthYear = Integer.parseInt(reader.readLine());
            char gender = reader.readLine().charAt(0);
            double rating = Double.parseDouble(reader.readLine());
            NobelLaureate.Science science = NobelLaureate.Science.valueOf(reader.readLine());

            laureates[i] = new NobelLaureate(name, surname, patronymic, birthYear, gender, rating, science);
            laureates[i].id = id;
        }
        reader.close();
        return laureates;
    }
    //--------------------File Functions---------------------//

    public static void main(String[] args) throws IOException
    {
        NobelLaureate[] laureates = null;
        NobelLaureate laureate;

        int menuPoint;
        boolean ASC;

        do
        {
            print50Enters();
            printAll(laureates);
            printMenu();

            menuPoint = getMenuPoint();
            String fileName;

            switch (menuPoint)
            {
                case 0:
                    System.out.println("Program will be finished");
                    break;
                case 1:
                    laureate = createLaureate(true);
                    laureates = addToEnd(laureates, laureate);
                    break;
                case 2:
                    System.out.print("Input File Name: ");
                    fileName = scanner.next();

                    printAllToFile(laureates, fileName);
                    break;
                case 3:
                    System.out.print("Input File Name: ");
                    fileName = scanner.next();

                    saveLaureatesToFile(laureates, fileName);
                    break;
                case 4:
                    System.out.print("Input File Name: ");
                    fileName = scanner.next();

                    laureates = loadLaureatesFromFile(fileName);
                    break;
                case 5:
                    System.out.print("In ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    sortByBirthYear(laureates, ASC);
                    break;
                case 6:
                    printAll(findMaleLaureates(laureates));
                    break;
                case 7:
                    printAll(findFemaleLaureates(laureates));
                    break;
                case 8:
                    System.out.println("Input min and max rating between " + getMinRating(laureates) + " and " + getMaxRating(laureates));

                    System.out.print("Min: ");
                    double minRating = scanner.nextDouble();

                    System.out.print("Max: ");
                    double maxRating = scanner.nextDouble();

                    printAll(getLaureatesBetweenMinMaxRating(laureates, minRating, maxRating));
                    break;
                default:
                    System.out.println("Unknown command");
                    break;
            }
            System.in.read();
        }
        while (menuPoint != 0);
    }
}
