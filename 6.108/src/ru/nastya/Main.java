package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int money;
        int q64 = 0;
        int q32 = 0;
        int q16 = 0;
        int q8 = 0;
        int q4 = 0;
        int q2 = 0;
        int q1 = 0;

        System.out.println("Input money");
        money = scanner.nextInt();

        while (money > 0)
        {
            if (money % 64 == 0)
            {
                money -= 64;
                q64++;
            }
            else if (money % 32 == 0)
            {
                money -= 32;
                q32++;
            }
            else if (money % 16 == 0)
            {
                money -= 16;
                q16++;
            }
            else if (money % 8 == 0)
            {
                money -= 8;
                q8++;
            }
            else if (money % 4 == 0)
            {
                money -= 4;
                q4++;
            }
            else if (money % 2 == 0)
            {
                money -= 2;
                q2++;
            }
            else
            {
                money -= 1;
                q1++;
            }
        }
        System.out.println("q64 = " + q64);
        System.out.println("q32 = " + q32);
        System.out.println("q16 = " + q16);
        System.out.println("q8 = " + q8);
        System.out.println("q4 = " + q4);
        System.out.println("q2 = " + q2);
        System.out.println("q1 = " + q1);
    }
}
