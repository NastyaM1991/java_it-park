package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main
{
    static int[] CreateMas()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input n: ");
        int n = scanner.nextInt();

        return new int[n];
    }

    static void FillMasRandom(int[] mas)
    {
        Random rnd = new Random();

        for (int i = 0; i < mas.length; i++)
        {
            mas[i] = rnd.nextInt(100);
        }
    }

    static void PrintMas(int[] mas)
    {
        for (int i = 0; i < mas.length; i++)
        {
            System.out.print(mas[i] + " ");
        }
        System.out.println();
    }

    static int SumOfMas(int[] mas)
    {
        int sum = 0;
        for (int i = 0; i < mas.length; i++)
        {
            sum += mas[i];
        }

        return sum;
    }

    static int MulOfMas(int[] mas)
    {
        int mul = 1;
        for (int i = 0; i < mas.length; i++)
        {
            mul *= mas[i];
        }

        return mul;
    }

    static int MinOfMas(int[] mas)
    {
        int min = mas[0];
        for (int i = 0; i < mas.length; i++)
        {
            if (mas[i] < min)
            {
                min = mas[i];
            }
        }

        return min;
    }

    static int MaxOfMas(int[] mas)
    {
        int max = mas[0];
        for (int i = 0; i < mas.length; i++)
        {
            if (mas[i] > max)
            {
                max = mas[i];
            }
        }

        return max;
    }

    static int FindInMas(int[] mas)
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input number");
        int number = scanner.nextInt();

        int index = -1;
        for (int i = 0; i < mas.length; i++)
        {
            if (mas[i] == number && index == -1)
            {
                index = i;
            }
        }

        return index;
    }

    static void IncreaseMas(int[] mas, int k)
    {
        for (int i = 0; i < mas.length; i++)
        {
            mas[i] *= k;
        }
    }

    static void ReverseMas(int[] mas)
    {
        int temp;
        for (int i = 0; i < mas.length/2; i++)
        {
            temp = mas[i];
            mas[i] = mas[mas.length-1-i];
            mas[mas.length-1-i] = temp;
        }
    }


    public static void main(String[] args)
    {
        int[] mas = CreateMas();
        FillMasRandom(mas);
        PrintMas(mas);

        System.out.println("Sum = " + SumOfMas(mas));

        System.out.println("Mul = " + MulOfMas(mas));

        System.out.println("Min = " + MinOfMas(mas));

        System.out.println("Max = " + MaxOfMas(mas));

        System.out.println("Index = " + FindInMas(mas));

        IncreaseMas(mas,2);
        PrintMas(mas);

        ReverseMas(mas);
        PrintMas(mas);
    }
}
