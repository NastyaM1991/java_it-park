package ru.nastya;

public class Main
{

    public static void main(String[] args)
    {
        int i = 100;
        int count = 0;

        while (count < 10)
        {
            if (i % 10 == 7 && i % 9 == 0)
            {
                System.out.println(i);
                count++;
            }
            i++;
        }
    }
}
