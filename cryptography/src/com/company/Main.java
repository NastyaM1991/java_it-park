package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n;

        System.out.print("input count letters in word: ");
        n = scanner.nextInt();

        int masSize = (int)Math.ceil(Math.sqrt(n));
        char[][] mas = new char[masSize][masSize];
        char[][] tempMas = new char[masSize][masSize];

        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                mas[i][j] = '.';
            }
        }

        //input word
        System.out.println("input word by one letter on row: ");
        for(int k=0;k<n;k++)
        {
            System.out.print("input current letter: ");

            int i = k/masSize;
            int j = k%masSize;

            mas[i][j] = scanner.next().charAt(0);
        }

        //display word in mas
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                System.out.print(mas[i][j]);
            }
            System.out.println();
        }

        //display word
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                if(mas[i][j]=='.') continue;
                System.out.print(mas[i][j]);
            }
        }
        System.out.println();

        System.out.println("////////////////////////////////");

        //turn word
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                tempMas[j][i] = mas[i][j];
            }
        }

        //put back in into mas
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                mas[i][j] = tempMas[i][j];
            }
        }

        //display mas
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                System.out.print(mas[i][j]);
            }
            System.out.println();
        }

        //display word
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                if(mas[i][j]=='.') continue;
                System.out.print(mas[i][j]);
            }
        }
        System.out.println();

        System.out.println("////////////////////////////////");

        //decrypt
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                mas[i][j] = tempMas[j][i];
            }
        }

        //display mas
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                System.out.print(mas[i][j]);
            }
            System.out.println();
        }

        //display word
        for (int i=0;i<masSize;i++)
        {
            for (int j=0;j<masSize;j++)
            {
                if(mas[i][j]=='.') continue;
                System.out.print(mas[i][j]);
            }
        }
    }
}
