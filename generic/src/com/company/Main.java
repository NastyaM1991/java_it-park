package com.company;

public class Main
{
    static void printList(MyList list)
    {
        for (int i = 0; i < list.getLength(); i++)
        {
            System.out.print(list.getAt(i) + " ");
        }
        System.out.println();
    }

    public static void main(String[] args)
    {
        MyList<Character> list = new MyList<Character>();

        list.add('a');
        list.add('b');
        list.add('c');
        printList(list);


        /*list.setAt(0, 999);
        printList(list);

        list.insertAt(1, 111);
        printList(list);

        list.removeAt(2);
        printList(list);
*/

    }
}
