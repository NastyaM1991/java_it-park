package com.company;

public class MyList<T>
{
    private T[] array;

    public MyList()
    {
        array = (T[]) new Object[0];
    }

    private void resizeArray(int newLength)
    {
        T[] newArray = (T[]) new Object[newLength];

        int actualLength = newLength < array.length ? newLength : array.length;

        for (int i = 0; i < actualLength; i++)
        {
            newArray[i] = array[i];
        }

        array = newArray;
    }

    public void add(T elem)
    {
        resizeArray(array.length + 1);
        array[array.length - 1] = elem;
    }

    public void removeAt(int index)
    {
        if (array.length > 0 && index >= 0 && index <= array.length - 1)
        {
            for (int i = index; i < array.length - 1; i++)
            {
                array[i] = array[i + 1];
            }

            resizeArray(array.length - 1);
        }
    }

    public void insertAt(int index, T elem)
    {
        if (array.length > 0 && index >= 0 && index <= array.length - 1)
        {
            resizeArray(array.length + 1);

            for (int i = array.length - 1; i > index; i--)
            {
                array[i] = array[i - 1];
            }
            array[index] = elem;
        }
    }

    public void clear()
    {
        array = (T[]) new Object[0];
    }

    public T getAt(int index)
    {
        try
        {
            return array[index];
        } catch (Exception e)
        {
            throw e;
        }
    }

    public void setAt(int index, T elem)
    {
        if (array.length > 0 && index >= 0 && index <= array.length - 1)
        {
            array[index] = elem;
        }
    }

    public int getLength()
    {
        return array.length;
    }


}
