package ru.nastya;

public class Main
{

    public static void main(String[] args)
    {
        int year = 0;
        double S = 100;
        double productivity = 20;
        double harvest = productivity * S;

        while (productivity <= 22)
        {
            productivity += productivity / 100 * 2.0;
            year++;
        }
        System.out.println("years = " + year);

        year = 0;
        while (S <= 120)
        {
            S += S / 100 * 5.0;
            year++;
        }
        System.out.println("years = " + year);

        year = 0;
        productivity = 20;
        S = 100;

        while (harvest <= 8000)
        {
            productivity += productivity / 100.0 * 2.0;
            S += S / 100.0 * 5.0;
            harvest = productivity * S;
            year++;
        }
        System.out.println("years = " + year);
    }
}
