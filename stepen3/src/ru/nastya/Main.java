package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int num;

        System.out.println("Input num");
        num = scanner.nextInt();

        while (num % 3 == 0)
        {
            num = num / 3;
        }

        if (num == 1)
        {
            System.out.println("YES");
        }
        else
        {
            System.out.println("NO");
        }
    }
}
