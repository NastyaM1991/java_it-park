package com.company;

public class Main
{

    static double Discriminant(double a, double b, double c)
    {
        return b * b - 4.0 * a * c;
    }

    static int Fact(int n)
    {
        int fact = 1;
        for (int i = 1; i <= n; i++)
        {
            fact *= i;
        }
        return fact;
    }

    static boolean Chet(int n)
    {
        return n % 2 == 0;
        /*if (n % 2 == 0)
        {
            return true;
        }
        else
        {
            return false;
        }*/
    }

    static void fromAtoB(int a, int b)
    {
        for (int i = a; i <= b; i++)
        {
            if (Chet(i) == true)
            {
                System.out.println(i);
            }
        }
    }

    static void PrintStars()
    {
        for (int i = 0; i < 80; i++)
        {
            System.out.print('*');
        }
    }

    static void PrintStars2(int n)
    {
        for (int i = 0; i < n; i++)
        {
            System.out.print('*');
        }
    }

    public static void main(String[] args)
    {
//        System.out.println((2*Fact(5)+3*Fact(8))/(Fact(6)+Fact(4)));
//        System.out.println(Chet(-19));
//        fromAtoB(-5,5);
//        PrintStars();
//        System.out.println();
//        PrintStars2(5);

    }
}
