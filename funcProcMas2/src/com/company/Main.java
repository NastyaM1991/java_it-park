package com.company;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Main
{
    static Random random = new Random();

    enum Cell
    {
        Empty,
        AliveShip,
        DeadShip,
        Miss
    }

    enum Step
    {
        User,
        Comp
    }

    static void ClearField(Cell[][] field)
    {
        for (int i = 0; i < field.length; i++)
        {
            for (int j = 0; j < field[i].length; j++)
            {
                field[i][j] = Cell.Empty;
            }
        }
    }

    static void FillFieldByShips(Cell[][] field, int countShips)
    {
        for (int k = 0; k < countShips; k++)
        {
            int i;
            int j;

            do
            {
                i = random.nextInt(field.length);
                j = random.nextInt(field[0].length);
            }
            while (field[i][j] != Cell.Empty);

            field[i][j] = Cell.AliveShip;
        }
    }

    static void PrintField(Cell[][] field, boolean isHideAliveShips)
    {
        for (int i = 0; i < field.length; i++)
        {
            for (int j = 0; j < field[i].length; j++)
            {
                switch (field[i][j])
                {
                    case Empty:
                        System.out.print(".");
                        break;
                    case AliveShip:
                        if (isHideAliveShips)
                        {
                            System.out.print(".");
                        }
                        else
                        {
                            System.out.print("K");
                        }
                        break;
                    case DeadShip:
                        System.out.print("X");
                        break;
                    case Miss:
                        System.out.print("O");
                        break;
                }
            }
            System.out.println();
        }
    }

    static void PrintUserField(Cell[][] field)
    {
        System.out.println("User Field");
        PrintField(field, false);
        System.out.println();
    }

    static void PrintCompField(Cell[][] field)
    {
        System.out.println("Comp Field");
        PrintField(field, true);
        System.out.println();
    }

    static Step UserShoot(Cell[][] field)
    {
        Scanner scanner = new Scanner(System.in);

        int i;
        int j;

        System.out.println("Player Shoot");
        do
        {
            System.out.print("Input i: ");
            i = scanner.nextInt() - 1;

            System.out.print("Input j: ");
            j = scanner.nextInt() - 1;
        }
        while (i < 0 || j < 0 || i > field.length - 1 || j > field[0].length - 1 || field[i][j] == Cell.Miss || field[i][j] == Cell.DeadShip);

        if (field[i][j] == Cell.AliveShip)
        {
            field[i][j] = Cell.DeadShip;
            return Step.User;
        }
        if (field[i][j] == Cell.Empty)
        {
            field[i][j] = Cell.Miss;
            return Step.Comp;
        }

        return null;
    }

    static Step CompShoot(Cell[][] field) throws IOException
    {
        int i;
        int j;

        System.out.println("Comp Shoot <Press Enter>");
        System.in.read();
        do
        {
            i = random.nextInt(field.length);
            j = random.nextInt(field[0].length);
        }
        while (field[i][j] == Cell.Miss || field[i][j] == Cell.DeadShip);

        if (field[i][j] == Cell.AliveShip)
        {
            field[i][j] = Cell.DeadShip;
            return Step.Comp;
        }
        if (field[i][j] == Cell.Empty)
        {
            field[i][j] = Cell.Miss;
            return Step.User;
        }

        return null;
    }

    static int GetCountAliveShips(Cell[][] field)
    {
        int countAlive = 0;

        for (int i = 0; i < field.length; i++)
        {
            for (int j = 0; j < field[i].length; j++)
            {
                if (field[i][j] == Cell.AliveShip)
                {
                    countAlive++;
                }
            }
        }

        return countAlive;
    }

    static void Print50Enters()
    {
        for (int i = 0; i < 50; i++)
        {
            System.out.println();
        }
    }

    static void PrintWinner(Step winner)
    {
        switch (winner)
        {
            case User:
                System.out.println("USER WIN");
                break;
            case Comp:
                System.out.println("COMP WIN");
                break;
        }
    }

    public static void main(String[] args) throws IOException
    {
        final int fieldSize = 3;

        Cell[][] userField = new Cell[fieldSize][fieldSize];
        Cell[][] compField = new Cell[fieldSize][fieldSize];

        ClearField(userField);
        ClearField(compField);

        int countShipsUser = fieldSize;
        int countShipsComp = fieldSize;

        FillFieldByShips(userField, countShipsUser);
        FillFieldByShips(compField, countShipsComp);

        boolean playGame = true;
        Step currentStep = random.nextInt(1000) > 500 ? Step.User : Step.Comp;
        Step winner = null;

        while (playGame)
        {
            Print50Enters();
            PrintUserField(userField);
            PrintCompField(compField);

            if (currentStep == Step.User)
            {
                currentStep = UserShoot(compField);
            }
            else if (currentStep == Step.Comp)
            {
                currentStep = CompShoot(userField);
            }

            countShipsUser = GetCountAliveShips(userField);
            countShipsComp = GetCountAliveShips(compField);

            if (countShipsComp == 0 || countShipsUser == 0)
            {
                winner = currentStep;
                playGame = false;
            }
        }

        Print50Enters();
        PrintUserField(userField);
        PrintCompField(compField);

        PrintWinner(winner);
    }
}
