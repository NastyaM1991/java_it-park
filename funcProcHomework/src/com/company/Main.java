package com.company;

import java.lang.reflect.Field;
import java.util.Scanner;

public class Main
{
    static void printMas(int[] mas)
    {
        for (int i = 0; i < mas.length; i++)
        {
            System.out.print(mas[i] + " ");
        }
        System.out.println();
    }

    static void fillMas(int[] height)
    {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < height.length; i++)
        {
            System.out.println("Input " + (i + 1));
            height[i] = scanner.nextInt();
        }
    }

    static double countBoys(int[] height)
    {
        int sum = 0;
        int count = 0;
        for (int i = 0; i < height.length; i++)
        {
            if (height[i] < 0)
            {
                sum += height[i];
                count++;
            }
        }
        return -(1.0 * sum / count);
    }

    static double countGirls(int[] height)
    {
        int sum = 0;
        int count = 0;
        for (int i = 0; i < height.length; i++)
        {
            if (height[i] > 0)
            {
                sum += height[i];
                count++;
            }
        }
        return 1.0 * sum / count;
    }

    static void findTwoSame(int[] mas)
    {
        for (int i = 0; i < mas.length - 1; i++)
        {
            for (int j = i + 1; j < mas.length; j++)
            {
                if (mas[i] == mas[j])
                {
                    System.out.println("index 1 = " + i + ", index 2 = " + j);
                    System.out.println("first = " + mas[i] + ", second = " + mas[j]);
                    i = mas.length; //чтобы внешний цикл прервался
                    break;
                }
            }
        }
    }

    static void findFirstAndSecond(int[] score)
    {
        int firstIndex = 0;
        int secondIndex = -1;

        for (int i = 0; i < score.length; i++)
        {
            if (score[i] > score[firstIndex])
            {
                secondIndex = firstIndex;
                firstIndex = i;
            }
            else if (score[i] < score[firstIndex] && (secondIndex == -1 || score[i] > score[secondIndex]))
            {
                secondIndex = i;
            }
        }

        System.out.println("First index = " + firstIndex);
        System.out.println("Second index = " + secondIndex);
    }

    public static void main(String[] args)
    {
//        Task 1
        /*int N = 22;
        int[] height = new int[N];

        fillMas(height);
        printMas(height);

        System.out.println("Average height boys = " + countBoys(height));
        System.out.println("Average height girls = " + countGirls(height));*/

//        Task 2
        /*int K = 5;
        int[] mas = new int[K];

        fillMas(mas);
        printMas(mas);
        findTwoSame(mas);*/

//        Task 3
        int M = 5;
        int[] score = new int[M];

        fillMas(score);
        printMas(score);
        findFirstAndSecond(score);
    }
}
