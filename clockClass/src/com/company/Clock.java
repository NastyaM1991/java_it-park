package com.company;

public class Clock
{
    private String name;
    private double time;
    private double weight;

    public Clock()
    {
        name = "unknown";
        time = 0;
        weight = 0;
    }

    public Clock(String name, double time, double weight)
    {
        this.name = name;
        this.time = time;
        this.weight = weight;
    }

    public Clock(Clock clock)
    {
        name = clock.name;
        time = clock.time;
        weight = clock.weight;
    }

    public String getStringInfo()
    {
        return "Name: " + name + " Time: " + time + " Weight: " + weight;
    }

    public double getTime()
    {
        return time;
    }

    public double getWeight()
    {
        return weight;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTime(double time)
    {
        if (time >= 0 && time <= 24)
        {
            this.time = time;
        }
    }

    public void setWeight(double weight)
    {
        if (weight > 0 && weight <= 500)
        {
            this.weight = weight;
        }
    }

    public void tickTack()
    {
        System.out.println(name + " Tick-Tack");
    }
}
