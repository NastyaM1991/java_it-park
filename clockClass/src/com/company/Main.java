package com.company;

public class Main
{

    public static void main(String[] args)
    {
        Clock clock1 = new Clock();
        Clock clock2 = new Clock("Casio", 1.30, 40);
        Clock clock3 = new Clock(clock2);
        Clock clock4 = new Clock("Swatch", clock2.getTime()*10, clock3.getWeight()+2.3);

        clock3.setTime(clock2.getTime()*10);
        clock3.setWeight(clock2.getWeight()*2);

        System.out.println(clock1.getStringInfo());
        System.out.println(clock2.getStringInfo());
        System.out.println(clock3.getStringInfo());
        System.out.println(clock4.getStringInfo());

        clock2.tickTack();
        clock4.tickTack();
    }
}
