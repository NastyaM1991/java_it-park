package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int number;
        int sum = 0;
        int digit;
        int count = 0;
        boolean digit3 = false;

        System.out.println("Input number");
        number = scanner.nextInt();

        System.out.println("Input digit");
        digit = scanner.nextInt();

        while (number != 0)
        {
            sum += number % 10;
            if (number % 10 == digit)
            {
                count++;
            }
            if (number%10 == 3)
            {
                digit3 = true;
            }
            number = number / 10;
        }
        System.out.println(sum);
        System.out.println(count + " digits");

        if (sum > 10)
        {
            System.out.println("sum > 10");
        }
        else
        {
            System.out.println("sum is not bigger than 10");
        }

        if (digit3 == true)
        {
            System.out.println("3");
        }
        else{
            System.out.println("no 3");
        }
    }
}
