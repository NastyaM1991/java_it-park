package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Football club
        /*System.out.println("Input football club name");
        String clubName = scanner.next();

        System.out.println(clubName.length());*/

        //Word symbols
        /*String str = "word";
        System.out.println(str.charAt(2));
        System.out.println(str.charAt(str.length()-1));*/

        //Яблоко
        /*String apple = "яблоко";

        StringBuffer stringBuffer = new StringBuffer(apple);
        String newApple = apple.substring(1,5);
        System.out.println(newApple);*/

        //Курсор
        /*String word = "курсор";
        String word2 = "танц";
        StringBuffer stringBuffer = new StringBuffer(word);

        word = word.substring(4,6);
        word = word2 + word;

        System.out.println(word);*/

        //Symbols in column
        /*String word = "word";
        for (int i = 0; i < word.length(); i++)
        {
            System.out.println(word.charAt(i));
        }*/

        //Reverse
        /*String word = "word";
        StringBuffer stringBuffer = new StringBuffer(word);
        System.out.println(stringBuffer.reverse());*/

        //Print * x100
        /*String stars = "*";

        for (int i = 0; i < 100; i++)
        {
            stars += '*';
        }
        System.out.println(stars);*/

        //Find 'o'
        System.out.println("Input string");
        String str = scanner.next();

        int countO = 0;
        int index = 0;

        while((index = str.indexOf('o', index)) != -1)
        {
            countO++;
            index++;
        }

        System.out.println(countO);
    }
}