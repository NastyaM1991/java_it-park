package ru.nastya;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int first, second, third, difference = 0;

        first = scanner.nextInt();
        second = scanner.nextInt();
        third = scanner.nextInt();

        if (first >= second && first >= third)
        {
            if (second < third)
            {
                difference = first - second;
            }
            else{
                difference = first - third;
            }
        }
        else if (second >= first && second >= third)
        {
            if (first < third)
            {

                difference = second - first;
            }
            else{
                difference = second - third;
            }
        }
        else if (third >= first && third >= second)
        {
            if (second < first)
            {

                difference = third - second;
            }
            else{
                difference = third - first;
            }
        }
        System.out.println(difference);
    }
}
