package com.company;

import java.io.*;
import java.util.Scanner;

public class Main
{
    static Scanner scanner = new Scanner(System.in);

    static class Food
    {
        static int LastId = 0;

        enum TypeOfFood
        {
            Drink,
            Fruit,
            Vegetable,
            Pastry,
            Meat
        }

        int Id;
        String Name;
        double Price;
        TypeOfFood FoodType;

        Food(String Name, double Price, TypeOfFood FoodType)
        {
            this.Name = Name;
            this.Price = Price;
            this.FoodType = FoodType;
        }
    }

    //--------------------System Functions---------------------//

    static void printMenu()
    {
        System.out.println("0 - Exit");
        System.out.println("1 - Add New Food");
        System.out.println("2 - Delete Food By Id");
        System.out.println("3 - Clear Array");
        System.out.println("4 - Update Food By Id");
        System.out.println("5 - Insert Food Into Position");
        System.out.println("6 - Print Food By Id");
        System.out.println("7 - Print Foods Between Min And Max Prices");
        System.out.println("8 - Sort Foods By Id");
        System.out.println("9 - Sort Foods By Price");
        System.out.println("10 - Print Foods To txt File");
        System.out.println("11 - Save Foods To data File");
        System.out.println("12 - Load Foods From data File");

    }

    static int inputMenuPoint()
    {
        System.out.print("Input menu point: ");
        return scanner.nextInt();
    }

    static void print50Enters()
    {
        for (int i = 0; i < 50; i++)
        {
            System.out.println();
        }
    }

    static Food[] resizeArray(Food[] oldFoods, int newLength)
    {
        Food[] newFoods = new Food[newLength];

        int minLength = newLength < oldFoods.length ? newLength : oldFoods.length;

        for (int i = 0; i < minLength; i++)
        {
            newFoods[i] = oldFoods[i];
        }
        return newFoods;
    }

    //--------------------System Functions---------------------//

    //--------------------CRUD Functions---------------------//

    static Food[] addToEnd(Food[] foods, Food food)
    {
        if (foods == null)
        {
            foods = new Food[1];
        }
        else
        {
            foods = resizeArray(foods, foods.length + 1);
        }
        foods[foods.length - 1] = food;

        return foods;
    }

    static Food[] deleteById(Food[] foods, int id)
    {
        int indexDel = getFoodIndexById(foods, id);

        if (indexDel == -1)
        {
            System.out.println("Delete impossible. Element not found");
            return foods;
        }

        Food[] newFoods = new Food[foods.length - 1];
        int newI = 0;

        for (int i = 0; i < foods.length; i++)
        {
            if (i != indexDel)
            {
                newFoods[newI] = foods[i];
                newI++;
            }
        }

        return newFoods;
    }

    static void updateFoodById(Food[] foods, int id, Food food)
    {
        int indexUpd = getFoodIndexById(foods, id);

        if (indexUpd == -1)
        {
            System.out.println("Update impossible. Element not found");
            return;
        }

        food.Id = id;
        foods[indexUpd] = food;
    }

    static Food[] insertFoodIntoPosition(Food[] foods, int position, Food food)
    {
        if (position < 1 || position > foods.length)
        {
            System.out.println("Insert impossible. Element not found");
            return foods;
        }

        int indexIns = position - 1;

        Food[] newFood = new Food[foods.length + 1];
        int newI = 0;

        for (int i = 0; i < foods.length; i++)
        {
            if (newI != indexIns)
            {
                newFood[newI] = foods[i];
                newI++;
            }
            else
            {
                newFood[newI] = food;
                newI++;
                i--;
            }
        }

        return newFood;
    }
    //--------------------CRUD Functions---------------------//

    //--------------------Ext Functions---------------------//

    static double getMinPrice(Food[] foods)
    {
        double minPrice = foods[0].Price;

        for (int i = 0; i < foods.length; i++)
        {
            if (foods[i].Price < minPrice)
            {
                minPrice = foods[i].Price;
            }
        }
        return minPrice;
    }

    static double getMaxPrice(Food[] foods)
    {
        double maxPrice = foods[0].Price;

        for (int i = 0; i < foods.length; i++)
        {
            if (foods[i].Price > maxPrice)
            {
                maxPrice = foods[i].Price;
            }
        }
        return maxPrice;
    }

    static Food[] getFoodsBetweenMaxMinPrice(Food[] foods, double minPrice, double maxPrice)
    {
        Food[] foundFoods = null;

        for (int i = 0; i < foods.length; i++)
        {
            if (foods[i].Price >= minPrice && foods[i].Price <= maxPrice)
            {
                foundFoods = addToEnd(foundFoods, foods[i]);
            }
        }
        return foundFoods;
    }

    static void sortById(Food[] foods, boolean ASC)
    {
        boolean sort;
        Food temp;
        int offset = 0;

        do
        {
            sort = true;

            for (int i = 0; i < foods.length - 1 - offset; i++)
            {
                boolean condition;
                if (ASC)
                {
                    condition = foods[i + 1].Id < foods[i].Id;
                }
                else
                {
                    condition = foods[i + 1].Id > foods[i].Id;
                }
                if (condition)
                {
                    sort = false;
                    temp = foods[i];
                    foods[i] = foods[i + 1];
                    foods[i + 1] = temp;
                }
            }
            offset++;
        }
        while (!sort);
    }

    static void sortByPrice(Food[] foods, boolean ASC)
    {
        boolean sort;
        Food temp;
        int offset = 0;

        do
        {
            sort = true;

            for (int i = 0; i < foods.length - 1 - offset; i++)
            {
                boolean condition;
                if (ASC)
                {
                    condition = foods[i + 1].Price < foods[i].Price;
                }
                else
                {
                    condition = foods[i + 1].Price > foods[i].Price;
                }
                if (condition)
                {
                    sort = false;
                    temp = foods[i];
                    foods[i] = foods[i + 1];
                    foods[i + 1] = temp;
                }
            }
            offset++;
        }
        while (!sort);
    }


    static void printSingle(Food food)
    {
        System.out.printf("%-4d%-10s%-10.2f%-10s\n", food.Id, food.Name, food.Price, food.FoodType);
    }

    static void printAll(Food[] foods)
    {
        System.out.printf("%-4s%-10s%-10s%-10s\n", "Id", "Name", "Price", "FoodType");

        if (foods == null)
        {
            System.out.println("Array is empty");
        }
        else if (foods.length == 0)
        {
            System.out.println("Array is empty");
        }
        else
        {
            for (int i = 0; i < foods.length; i++)
            {
                printSingle(foods[i]);
            }
        }
        System.out.println("------------------------------");
    }

    static Food getFoodById(Food[] foods, int id)
    {
        int indexPrt = getFoodIndexById(foods, id);

        if (indexPrt == -1)
        {
            return null;
        }

        return foods[indexPrt];
    }

    static Food createFood(boolean newId)
    {
        System.out.print("Input Name: ");
        String name = scanner.next();

        System.out.print("Input Price: ");
        double price = scanner.nextDouble();

        System.out.print("Input Food Type: ");
        Food.TypeOfFood foodType = Food.TypeOfFood.valueOf(scanner.next());

        Food food = new Food(name, price, foodType);

        if (newId)
        {
            Food.LastId++;
            food.Id = Food.LastId;
        }

        return food;
    }

    static int getFoodIndexById(Food[] foods, int id)
    {
        for (int i = 0; i < foods.length; i++)
        {
            if (foods[i].Id == id)
            {
                return i;
            }
        }
        return -1;
    }
    //--------------------Ext Functions---------------------//

    //--------------------File Functions---------------------//

    static void printAllToFile(Food[] foods, String fileName) throws FileNotFoundException
    {
        PrintStream stream = new PrintStream(fileName);

        stream.printf("%-4s%-10s%-10s%-10s\n", "Id", "Name", "Price", "FoodType");
        stream.println();

        if (foods == null)
        {
            stream.println("Array is empty");
        }
        else if (foods.length == 0)
        {
            stream.println("Array is empty");
        }
        else
        {
            for (int i = 0; i < foods.length; i++)
            {
                stream.printf("%-4d%-10s%-10.2f%-10s\n", foods[i].Id, foods[i].Name, foods[i].Price, foods[i].FoodType);
                stream.println();
            }
        }
        stream.close();
    }

    static void saveFoodsToFile(Food[] foods, String fileName) throws FileNotFoundException
    {
        PrintStream stream = new PrintStream(fileName);

        stream.println(foods.length);
        stream.println(Food.LastId);

        for (int i = 0; i < foods.length; i++)
        {
            stream.println(foods[i].Id);
            stream.println(foods[i].Name);
            stream.println(foods[i].Price);
            stream.println(foods[i].FoodType);
        }

        stream.close();
    }

    static Food[] loadFoodsFromFile(String fileName) throws IOException
    {
        Food[] foods;
        int countFoods;

        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        countFoods = Integer.parseInt(reader.readLine());
        Food.LastId = Integer.parseInt(reader.readLine());

        foods = new Food[countFoods];

        for (int i = 0; i < foods.length; i++)
        {
            int id = Integer.parseInt(reader.readLine());
            String name = reader.readLine();
            double price = Double.parseDouble(reader.readLine());
            Food.TypeOfFood foodType = Food.TypeOfFood.valueOf(reader.readLine());

            foods[i] = new Food(name, price, foodType);
            foods[i].Id = id;
        }
        reader.close();
        return foods;
    }

    //--------------------File Functions---------------------//

    public static void main(String[] args) throws IOException
    {
        Food[] foods = null;
        int menuPoint;

        Food food;
        int id;
        boolean ASC;
        String fileName;

        do
        {
            print50Enters();
            printAll(foods);
            printMenu();

            menuPoint = inputMenuPoint();

            switch (menuPoint)
            {
                case 0:
                    System.out.println("Program will be finished");
                    break;
                case 1:
                    food = createFood(true);
                    foods = addToEnd(foods, food);
                    break;
                case 2:
                    System.out.print("Input id: ");
                    id = scanner.nextInt();
                    foods = deleteById(foods, id);
                    break;
                case 3:
                    foods = null;
                    break;
                case 4:
                    System.out.print("Input id: ");
                    id = scanner.nextInt();

                    food = createFood(false);

                    updateFoodById(foods, id, food);
                    break;
                case 5:
                    System.out.print("Input position: ");
                    int position = scanner.nextInt();

                    food = createFood(true);

                    foods = insertFoodIntoPosition(foods, position, food);
                    break;
                case 6:
                    System.out.print("Input id: ");
                    id = scanner.nextInt();
                    food = getFoodById(foods, id);

                    if (food == null)
                    {
                        System.out.println("Print impossible. Element not found");
                    }
                    else
                    {
                        printSingle(food);
                    }
                    break;
                case 7:
                    System.out.println("Input min and max prices between " + getMinPrice(foods) + " and " + getMaxPrice(foods));

                    System.out.print("Min: ");
                    double minPrice = scanner.nextDouble();

                    System.out.print("Max: ");
                    double maxPrice = scanner.nextDouble();

                    printAll(getFoodsBetweenMaxMinPrice(foods, minPrice, maxPrice));
                    break;
                case 8:
                    System.out.print("In ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    sortById(foods, ASC);
                    break;
                case 9:
                    System.out.print("In ascending order(true or false): ");
                    ASC = scanner.nextBoolean();

                    sortByPrice(foods, ASC);
                    break;
                case 10:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    printAllToFile(foods, fileName);
                    break;
                case 11:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    saveFoodsToFile(foods, fileName);
                    break;
                case 12:
                    System.out.print("Input file name: ");
                    fileName = scanner.next();

                    foods = loadFoodsFromFile(fileName);
                    break;
                default:
                    System.out.println("Unknown command");
                    break;
            }
            System.in.read();
        }
        while (menuPoint != 0);
    }
}
