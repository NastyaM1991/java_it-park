package com.company;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args) throws IOException
    {
        Scanner scanner = new Scanner(System.in);
        Random rnd = new Random();

        int n = 2;
        int m = 2;
        int countPairs = n * m / 2;
        char[][] closeField = new char[n][m];
        char[][] openField = new char[n][m];

        final char STAR_SYMBOL = '*';
        final int A_CODE = 65;
        final int Z_CODE = 90;

        int i1, j1;
        int i2, j2;
        boolean hasStarSymbol;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                closeField[i][j] = STAR_SYMBOL;
                openField[i][j] = STAR_SYMBOL;
            }
        }

        for (int k = 0; k < countPairs; k++)
        {
            char currentSymbol = (char) (Math.random() * (Z_CODE + 1 - A_CODE) + A_CODE);
            for (int kk = 0; kk < 2; kk++)
            {
                int ii, jj;

                do
                {
                    ii = rnd.nextInt(n);
                    jj = rnd.nextInt(m);
                }
                while (openField[ii][jj] != STAR_SYMBOL);

                openField[ii][jj] = currentSymbol;
            }
        }


        do
        {
            for (int i = 0; i < 50; i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(closeField[i][j] + " ");
                }
                System.out.println();
            }

            do
            {
                System.out.print("Input i1: ");
                i1 = scanner.nextInt();

                System.out.print("Input j1: ");
                j1 = scanner.nextInt();
            }
            while (closeField[i1][j1] != STAR_SYMBOL);

            closeField[i1][j1] = openField[i1][j1];


            for (int i = 0; i < 50; i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(closeField[i][j] + " ");
                }
                System.out.println();
            }

            do
            {
                System.out.print("Input i2: ");
                i2 = scanner.nextInt();

                System.out.print("Input j2: ");
                j2 = scanner.nextInt();

            }
            while (closeField[i2][j2] != STAR_SYMBOL);

            closeField[i2][j2] = openField[i2][j2];


            for (int i = 0; i < 50; i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(closeField[i][j] + " ");
                }
                System.out.println();
            }

            System.in.read();

            if (closeField[i1][j1] != closeField[i2][j2] || i1 == i2 && j1 == j2)
            {
                closeField[i1][j1] = STAR_SYMBOL;
                closeField[i2][j2] = STAR_SYMBOL;
            }

            hasStarSymbol = false;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (closeField[i][j] == STAR_SYMBOL)
                    {
                        hasStarSymbol = true;
                    }
                }
            }
        }
        while (hasStarSymbol == true);

        for (int i = 0; i < 50; i++)
        {
            System.out.println();
        }

        System.out.println("!!!YOU WIN!!!");

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(closeField[i][j] + " ");
            }
            System.out.println();
        }
    }
}
