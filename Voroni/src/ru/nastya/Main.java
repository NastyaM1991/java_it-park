package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int voroni, last, twoLast;

        System.out.print("Введите количество ворон ");
        voroni = scanner.nextInt();

        last = voroni % 10;
        twoLast = voroni % 100;
        
        if (last == 0 || last >= 5 && last <= 9 || twoLast >= 11 && twoLast <= 13)
        {
            System.out.print(voroni + " ворон");
        }
        else if (last == 1)
        {
            System.out.print(voroni + " ворона");
        }
        else
        {
            System.out.print(voroni + " вороны");
        }
    }
}
