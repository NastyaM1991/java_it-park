package com.company;

import java.util.Scanner;

/**
 * Белочка собрала в лесу N шишек c орешками. Белочка очень привередливо выбирала шишки, и брала только те,
 * в которых ровно M орешков. Также известно, что для пропитания зимой ей необходимо не менее K орешков.
 * Определите, хватит ли на зиму орешков белочке.
 */
public class Nuts
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input kolvoSH");
        int kolvoSh = scanner.nextInt();

        System.out.println("Input kolvoSobranOreh");
        int kolvoSobranOreh = scanner.nextInt();

        System.out.println("Input kolvoNuzhnOreh");
        int kolvoNuzhnOreh = scanner.nextInt();

        if (kolvoSh * kolvoSobranOreh >= kolvoNuzhnOreh)
        {
            System.out.println("YES");
        }
        else
        {
            System.out.println("NO");
        }
    }
}
