package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        Random rnd = new Random();

        int n = 10;
        int[] mas = new int[n];

        for (int i = 0; i < n; i++)
        {
            mas[i] = rnd.nextInt(100);
        }

        for (int i = 0; i < n; i++)
        {
            System.out.print(mas[i] + " ");
        }

        /*int temp;
        int spin = 0;
        boolean sort;

        do
        {
            sort = true;
            for (int i = 0; i < n - 1 - spin; i++)
            {
                if (mas[i + 1] < mas[i])
                {
                    temp = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = temp;

                    sort = false;
                }
            }
            spin++;
        }
        while (sort == false);*/

/*        int indexMax;
        int max;
        int temp;

        for (int i = 0; i < n; i++)
        {
            indexMax = i;
            max = mas[i];

            for (int j = i + 1; j < n; j++)
            {
                if (mas[j] > max)
                {
                    max = mas[j];
                    indexMax = j;
                }
            }

            temp = mas[i];
            mas[i] = mas[indexMax];
            mas[indexMax] = temp;
        }*/

        int temp;
        for (int i = 1; i < n; i++)
        {
            temp = mas[i];
            for (int j = i - 1; j >= 0 && mas[j] > temp; j--)
            {
                mas[j + 1] = mas[j];
                mas[j] = temp;
            }
        }

        System.out.println();
        for (int i = 0; i < n; i++)
        {
            System.out.print(mas[i] + " ");
        }
    }
}
