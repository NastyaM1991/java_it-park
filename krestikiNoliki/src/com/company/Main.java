package com.company;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        final char NO_SYMBOL = '.';

        int n = 3;
        int m = 3;

        char[][] field = new char[n][m];

        boolean winX = false;

        int i1, j1;
        int i2, j2;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                field[i][j] = NO_SYMBOL;
            }
        }

        do
        {
            for (int i = 0; i < 50; i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(field[i][j] + " ");
                }
                System.out.println();
            }

            //first player
            do
            {
                System.out.print("Input i1: ");
                i1 = scanner.nextInt();

                System.out.print("Input j1: ");
                j1 = scanner.nextInt();
            }
            while (field[i1][j1] != NO_SYMBOL);

            field[i1][j1] = 'X';

            for (int i = 0; i < 50; i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(field[i][j] + " ");
                }
                System.out.println();
            }

            //check

            if (field[0][0] == field[1][0] && field[1][0] == field [2][0] && field[0][0] == 'X' ||
                    field[0][1] == field[1][1] && field[1][1] == field [2][1] && field[0][1] == 'X' ||
                    field[0][2] == field[1][2] && field[1][2] == field [2][2] && field[0][2] == 'X' ||
                    field[0][0] == field[0][1] && field[0][1] == field [0][2] && field[0][0] == 'X' ||
                    field[1][0] == field[1][1] && field[1][1] == field [1][2] && field[1][0] == 'X' ||
                    field[2][0] == field[2][1] && field[2][1] == field [2][2] && field[2][0] == 'X' ||
                    field[0][0] == field[1][1] && field[1][1] == field [2][2] && field[0][0] == 'X' ||
                    field[2][0] == field[1][1] && field[1][1] == field [0][2] && field[2][0] == 'X')
            {
                winX = true;
                break;
            }

            //second player
            do
            {
                System.out.print("Input i2: ");
                i2 = scanner.nextInt();

                System.out.print("Input j2: ");
                j2 = scanner.nextInt();

            }
            while (field[i2][j2] != NO_SYMBOL);

            field[i2][j2] = 'O';

            for (int i = 0; i < 50; i++)
            {
                System.out.println();
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    System.out.print(field[i][j] + " ");
                }
                System.out.println();
            }

            //check

            if (field[0][0] == field[1][0] && field[1][0] == field [2][0] && field[0][0] == 'O' ||
                    field[0][1] == field[1][1] && field[1][1] == field [2][1] && field[0][1] == 'O' ||
                    field[0][2] == field[1][2] && field[1][2] == field [2][2] && field[0][2] == 'O' ||
                    field[0][0] == field[0][1] && field[0][1] == field [0][2] && field[0][0] == 'O' ||
                    field[1][0] == field[1][1] && field[1][1] == field [1][2] && field[1][0] == 'O' ||
                    field[2][0] == field[2][1] && field[2][1] == field [2][2] && field[2][0] == 'O' ||
                    field[0][0] == field[1][1] && field[1][1] == field [2][2] && field[0][0] == 'O' ||
                    field[2][0] == field[1][1] && field[1][1] == field [0][2] && field[2][0] == 'O')
            {
                break;
            }
        }
        while (true);

        if (winX == true)
        {
            System.out.println("WIN X");
        }
        else
        {
            System.out.println("WIN O");
        }
    }
}
