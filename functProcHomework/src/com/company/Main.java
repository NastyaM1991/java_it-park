package com.company;

public class Main
{

    static double max(double a, double b)
    {
        if (a > b)
        {
            return a;
        }
        else
        {
            return b;
        }
    }

    static int countDigits(int number)
    {
        int digits = 0;
        while (number > 0)
        {
            number /= 10;
            digits++;
        }

        return digits;
    }

    static int[] makeMas(int number)
    {
        int digits = countDigits(number);

        int[] numOfDigits = new int[digits];

        for (int i = 0; i < digits; i++)
        {
            numOfDigits[i] = number % 10;
            number /= 10;
        }

        return numOfDigits;
    }

    static boolean isHappy(int number)
    {
        int firstSum = 0;
        int secondSum = 0;
        int digits = countDigits(number);

        int[] numOfDigits = makeMas(number);

        for (int i = 0; i < digits / 2; i++)
        {
            firstSum += numOfDigits[i];
            secondSum += numOfDigits[digits - 1 - i];
        }

        return firstSum == secondSum;
    }

    static void Square(double a, double b)
    {
        System.out.println(a * b);
    }

    static double Perimeter(double a, double b)
    {
        return 2 * (a + b);
    }

    public static void main(String[] args)
    {
        //max of 6
        int num1 = 7;
        int num2 = 2;
        int num3 = 5;
        int num4 = 4;
        int num5 = 5;
        int num6 = 6;

        System.out.println("MAX");
        System.out.println(max(max(max(num1, num2), max(num3, num4)), max(num5, num6)));

        System.out.println("IS HAPPY");
        System.out.println(isHappy(45345));
        System.out.println(isHappy(453454));
        System.out.println(isHappy(453345));
        System.out.println(isHappy(4));
        System.out.println(isHappy(4545));

        System.out.println("SQUARE");
        Square(3, 4);

        System.out.println("PERIMETER");
        System.out.println(Perimeter(3, 4));
    }
}
