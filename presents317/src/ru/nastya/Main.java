package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int iWeight;
        int mWeight;
        int pWeight;
        int needWeight;

        iWeight = scanner.nextInt();
        mWeight = scanner.nextInt();
        pWeight = scanner.nextInt();
        needWeight = scanner.nextInt();

        int variants = 0;
        final int maxKolvoI = needWeight / iWeight;
        int kolvoI = 0;
        while (kolvoI <= maxKolvoI)
        {
            int kolvoM = 0;
            final int maxKolvoM = (needWeight - kolvoI * iWeight) / mWeight;
            while (kolvoM <= maxKolvoM)
            {
                int kolvoP = (needWeight - kolvoI * iWeight - kolvoM * mWeight) / pWeight;
                if (kolvoI * iWeight + kolvoM * mWeight + kolvoP * pWeight == needWeight)
                {
                    variants++;
                }
                kolvoM++;
            }
            kolvoI++;
        }
        System.out.println(variants);
    }
}
