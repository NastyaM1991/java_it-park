package com.company;

public class Main
{

    public static void main(String[] args)
    {
        Cat cat1 = new Cat();
        Cat cat2 = new Cat("Fluffy", 10, 8.5);
        Cat cat3 = new Cat(cat2);
        Cat cat4 = new Cat("Halk", cat3.getAge() + 2, cat3.getWeight() * 2);

        System.out.println(cat1.getStringInfo());
        System.out.println(cat2.getStringInfo());
        System.out.println(cat3.getStringInfo());
        System.out.println(cat4.getStringInfo());

    }
}
