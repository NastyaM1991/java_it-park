package com.company;

public class Cat
{
    private String name;
    private int age;
    private double weight;

    public Cat()
    {
        name = "unname";
        age = 0;
        weight = 0;
    }

    public Cat(String name, int age, double weight)
    {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public Cat(Cat cat)
    {
        name = cat.name;
        age = cat.age;
        weight = cat.weight;
    }

    public String getStringInfo()
    {
        return "Name: " + name + " Age: " + age + " Weight: " + weight;
    }

    public int getAge()
    {
        return age;
    }

    public double getWeight()
    {
        return weight;
    }

    public String getName()
    {
        return name;
    }

    public void setAge(int age)
    {
        if (age > 0 && age <= 25)
        {
            this.age = age;
        }
    }

    public void setWeight(double weight)
    {
        if (weight > 0 && weight <= 20)
        {
            this.weight = weight;
        }
    }
}
