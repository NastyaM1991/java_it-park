package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int quantity;
        int max;
        int min;
        int currentNumber;

        System.out.print("Input quantity ");
        quantity = scanner.nextInt();

        if (quantity != 0)
        {
            max = Integer.MIN_VALUE;
            min = Integer.MAX_VALUE;

            int i = 0;
            while (i < quantity)
            {
                System.out.printf("Input " + (i + 1) + " number ");
                currentNumber = scanner.nextInt();

                if (currentNumber > max)
                {
                    max = currentNumber;
                }
                if (currentNumber < min)
                {
                    min = currentNumber;
                }
                i++;
            }
            System.out.println("max = " + max);
            System.out.println("min = " + min);
        }
        else
        {
            System.out.println("!!! quantity > 0 !!!");
        }
    }
}
