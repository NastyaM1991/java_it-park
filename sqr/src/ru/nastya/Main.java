package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int num;
        int square = 0;

        System.out.println("Input num");
        num = scanner.nextInt();

        int i = 1;
        while (i <= 2 * num - 1)
        {
            square += i;
            i += 2;
        }
        System.out.println(square);
    }
}
