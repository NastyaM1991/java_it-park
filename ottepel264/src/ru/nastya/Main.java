package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int n;
        int days = 0;
        int maxDays = 0;
        int currentDay;

        //System.out.println("Input n");
        n = scanner.nextInt();

        int i = 0;
        while (i < n)
        {
            //System.out.println("Input number");
            currentDay = scanner.nextInt();

            if (currentDay > 0)
            {
                days++;
                if (days > maxDays)
                {
                    maxDays = days;
                }
            }
            else
            {
                days = 0;
            }
            i++;
        }
        System.out.println(maxDays);
    }
}
