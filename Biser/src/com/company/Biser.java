package com.company;

import java.util.Scanner;

/**
 * В шкатулке хранится разноцветный бисер (или бусины). Все бусины имеют одинаковую форму, размер и вес.
 * Бусины могут быть одного из N различных цветов. В шкатулке много бусин каждого цвета.
 * Требуется определить минимальное число бусин, которые можно не глядя вытащить из шкатулки так,
 * чтобы среди них гарантированно были две бусины одного цвета
 */
public class Biser
{

    public static void main(String[] args)
    {
        System.out.println("Input number of colors");
        Scanner scanner = new Scanner(System.in);
        int colors = scanner.nextInt();
        System.out.println("You need to take " + (colors + 1));
    }
}