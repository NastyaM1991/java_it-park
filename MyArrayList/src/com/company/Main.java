package com.company;

import java.util.ArrayList;

public class Main
{
    static void printList(ArrayList<Integer> list)
    {
        for (int i = 0; i < list.size(); i++)
        {
            System.out.print(list.get(i) + " ");
        }
        System.out.println();
    }

    public static void main(String[] args)
    {
        ArrayList<Integer> list = new ArrayList<Integer>();

        list.add(12);
        list.add(7);
        list.add(-2);
        printList(list);

        list.remove(1);
        printList(list);

        list.set(1, 10);
        printList(list);
        
    }
}
