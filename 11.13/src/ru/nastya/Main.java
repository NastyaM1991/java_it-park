package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int n = 10;

        int[] mas = new int[n];

        for (int i = 0; i < n; i++)
        {
            mas[i] = i + 1;
        }

        System.out.println("Input index");
        int index = scanner.nextInt();

        System.out.println(mas[index]);
    }
}
