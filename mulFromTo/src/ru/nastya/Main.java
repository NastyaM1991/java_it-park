package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int a;
        int b;

        System.out.println("Input a");
        a = scanner.nextInt();

        System.out.println("Input b");
        b = scanner.nextInt();

        if (b >= a)
        {
            int mul = 1;
            int i = a;
            while (i <= b)
            {
                mul *= i;
                i++;
            }
            System.out.println(mul);
        }
        else
        {
            System.out.println("b >= a");
        }
    }
}
