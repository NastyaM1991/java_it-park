package com.company;

public class Main
{

    public static void main(String[] args)
    {
        int n = 5, m = 5;
        int mas[][] = new int[n][m];


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (i >= j)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("---------------");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (i <= j)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (j <= m - 1 - i)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (j == (m - 1) / 2)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (i == (n - 1) / 2)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (i == (n - 1) / 2 || j == (m - 1) / 2)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("------------");


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (j % 2 == 0)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (i % 2 == 0)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (i == j || j == m - 1 - i)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                /*if (i % 2 == 0 && j % 2 == 0 || i % 2 != 0 && j % 2 != 0)
                {
                    mas[i][j] = 1;
                }*/
                if ((i + j) % 2 == 0)
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if ((j <= i || j >= m - 1 - i) && (j >= i || j <= m - 1 - i))
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if ((i <= j || i >= n - 1 - j) && (i >= j || i <= n - 1 - j))
                {
                    mas[i][j] = 1;
                }
                else
                {
                    mas[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");


        int n2 = 6;
        int m2 = 6;

        int mas2[][] = new int[n2][m2];
        for (int i = 0; i < n2; i++)
        {
            for (int j = 0; j < m2; j++)
            {
                if (i + j > m2 - 1)
                {
                    mas2[i][j] = i + j + 1 - m2;
                }
                else
                {
                    mas2[i][j] = i + j + 1;
                }
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(mas2[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("------------");


        System.out.println("СПИРАЛЬ");

        int num = 0;
        int y = 0;
        int x = 0;
        int stepX = 1;
        int stepY = 1;
        int steps = n;

        while (num < n * m)
        {
            for (int i = 0; i < steps; i++)
            {
                mas[y][x] = ++num;
                x += stepX;
            }

            x -= stepX;
            y += stepY;

            steps--;

            for (int i = 0; i < steps; i++)
            {
                mas[y][x] = ++num;
                y += stepY;
            }

            y -= stepY;
            x -= stepX;

            stepX = -stepX;
            stepY = -stepY;
        }


        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                System.out.print(String.format("%2d", mas[i][j]) + " ");
            }
            System.out.println();
        }

        System.out.println("------------");
    }
}
