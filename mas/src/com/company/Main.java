package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        Random rnd = new Random();

        int n;
        int findNumber, findIndex;
        int[] mas;

        System.out.print("input n: ");
        n = scanner.nextInt();

        mas = new int[n];

        for (int i = 0; i < n; i++)
        {
            mas[i] = rnd.nextInt(100);
        }

        boolean sort;
        int spin = 0;
        int temp;

        do
        {
            sort = true;

            for (int i = 0; i < n - 1 - spin; i++)
            {
                if (mas[i + 1] < mas[i])
                {
                    temp = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = temp;
                    sort = false;
                }
            }
            spin++;

        } while (sort == false);


        for (int i = 0; i < n; i++)
        {
            System.out.print(mas[i] + " ");
        }
        System.out.println();

        System.out.print("input findNumber: ");
        findNumber = scanner.nextInt();

        int countSpin = 0;
        findIndex = -1;
        int leftPos = 0;
        int rightPos = n;
        int middlePos;
        while (rightPos - leftPos > 1)
        {
            countSpin++;

            middlePos = (rightPos + leftPos) / 2;
            if (mas[middlePos] == findNumber)
            {
                findIndex = middlePos;
                break;
            }
            else if (findNumber < mas[middlePos])
            {
                rightPos = middlePos;
            }
            else if (findNumber > mas[middlePos])
            {
                leftPos = middlePos;
            }
        }

        /*for (int i=0;i<n;i++)
        {
            countSpin++;
            if(mas[i]==findNumber)
            {
                findIndex=i;
                break;
            }
        }*/
        System.out.println("findIndex = " + findIndex);
        System.out.println("countSpin = " + countSpin);

        //reverse

        /*for (int i = 0; i < n / 2; i++)
        {
            temp = mas[i];
            mas[i] = mas[n - 1 - i];
            mas[n - 1 - i] = temp;
        }

        for (int i = 0; i < n; i++)
        {
            System.out.print(mas[i] + " ");
        }
        System.out.println();*/
        //delete

        int deleteIndex;
        System.out.print("input deleteIndex: ");
        deleteIndex = scanner.nextInt();

        for (int i = deleteIndex; i < n - 1; i++)
        {
            mas[i] = mas[i + 1];
        }
        n--;

        for (int i = 0; i < n; i++)
        {
            System.out.print(mas[i] + " ");
        }

        //search

        for (int i = 0; i < n-1; i++)
        {
            for (int j = i+1; j < n; j++)
            {
                if (mas[i] == mas[j])
                {
                    System.out.println(mas[i] + " = " + mas[j] + " " + i + " " + j);
                }
            }
        }
    }
}
