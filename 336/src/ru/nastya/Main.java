package ru.nastya;

import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);

        int currentButton;
        int quantityOfAllButtons;
        int floor = 1;
        int maxFloor = 1;
        int minFloor = 1;

        System.out.println("Input quantity");
        quantityOfAllButtons = scanner.nextInt();

        for (int i = 0; i < quantityOfAllButtons; i++)
        {
            System.out.println("Input " + (i + 1) + " button");
            currentButton = scanner.nextInt();

            if (currentButton == 1)
            {
                floor++;
            }
            else
            {
                floor--;
            }
            if (floor > maxFloor)
            {
                maxFloor = floor;
            }
            else if (floor < minFloor)
            {
                minFloor = floor;
            }
        }
        System.out.println((maxFloor - minFloor + 1));
    }
}
